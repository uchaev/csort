<?php
/**
 * Шаблон по умолчанию
 *
 * @package WordPress
 * @subpackage csort
 * @since csort 1.0
 */

get_header(); ?>
<div class="info-page">
    <div class="info-page__wrapper">
        <div class="info-page__main">
            <div class="text-content">
                <div class="error-message">
                    Ошибка... ;(
                </div>
                <div class="category__wrapper category__wrapper_intro">
                    <?php
                    $args = array(
                        'post_type' => 'catalog',
                        'publish' => true,
                        'numberposts' => -1,
                        'orderby' => 'date',
                        'order' => 'ASC',
                        'limit' => '4',
                    );
                    ?>
                    <?php $index = 0; $catalog = new WP_Query($args); if ($catalog->have_posts()): while ($catalog->have_posts()): $catalog->the_post(); ?>
                        <div class="category-card category-card_intro category__item">
                            <div class="category-card__wrapper">
                                <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                    <div class="category-card__image">
                                        <img src="<?php echo types_render_field('catalog_img',array('url'=>'true'))?>" alt="<?php the_title();?>" title="<?php the_title();?>">
                                    </div>
                                    <div class="category-card__desc">
                                        <span><?php the_title();?><i> </i></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php endwhile; endif; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
