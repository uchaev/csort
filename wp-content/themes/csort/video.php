<?php
/**
 * Template Name: Видео галерея
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
<div class="video">
    <div class="video__title">
        <h1 class="title title_color title_large"><?php the_title(); ?></h1>
    </div>
    <div class="video__list">
        <?php
        $args = array(
            'post_type' => 'videos',
            'orderby' => 'date',
            'numberposts' => -1,
            'order' => 'DESC',
        );
        ?>

        <?php $videos = new WP_Query($args); while ($videos->have_posts()) {
            $videos->the_post(); ?>
            <div class="video__list-section">
                <div class="video-item video__list-item">
                    <div class="video-item__title"><span><?php the_title(); ?></span></div>
                    <div class="video-item__content">
                        <?php if (types_render_field('videos_url', array('raw' => 'true')) != NULL) { ?>
                            <video controls width="100%" src="<?php echo types_render_field('videos_url', array('raw' => 'true')) ?>">
                            </video>
                        <?php } else { ?>
                            <?php echo types_render_field('videos_code', array('raw' => 'true')) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php }
        wp_reset_postdata(); ?>

    </div>
</div>
<?php get_footer(); ?>
