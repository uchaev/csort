<?php
/**
 * Template Name: Работа в компании
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
<div class="modal" id="modal01" style="display: none;">
    <div class="modal__wrapper">
        <div class="modal__header modal__header_price">
            Работа в компании
        </div>
        <div js-content-main class="modal__content">
            <form js-form action="#">
				<input name="form_type" type="hidden" value="job"/>
				<input name="form_title" type="hidden" value="Работа в компании"/>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Ваше ФИО</span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="product" type="text" hidden value="продукт">
                        <input name="personal" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> E-mail</span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="email" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Телефон <sup>*</sup> </span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="phone" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row modal__row_button">
                    <div class="modal__grid modal__grid_button">
                        <button type="submit" class="button button_form">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
        <div js-content-succes class="modal__content modal__content_hidden">
            <div js-form-row class="modal__row">
                <div class="modal__grid modal__grid_success">
                    <span> Спасибо за заявку</span>
                </div>
            </div>
        </div>
        <div class="modal__footer">
            <span> Оставьте Ваши координаты и мы свяжемся с Вами в ближайшее время </span>
        </div>
    </div>
</div>
<div class="info-page">
	<div class="info-page__title"><h1 class="title title_color title_large title_regular"><?php the_title();?> «СиСорт»</h1></div>
	<div class="info-page__wrapper">
		<div class="info-page__main">
			<div class="text-content">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="info-page__aside">
			<div class="info-page__aside-image">
				<img src="<?php bloginfo('template_url'); ?>/images/main/promo-job-1.jpg" alt="<?php the_title();?>">
			</div>
			<div class="info-page__aside-image">
				<img src="<?php bloginfo('template_url'); ?>/images/main/promo-job-2.jpg" alt="<?php the_title();?>">
			</div>
		</div>
	</div>
	<div class="info-page__vacancies">
        <script class="zarplataWidgetLoader" type="text/javascript">!function(t,e){t.zarplataWidget_companyId="2574697",t.zarplateWidget_linkColor="#ec4125",t.zarplataWidget_buttonColor="#ec4125",t.zarplataWidget_fontName="Trebuchet MS, Helvetica, sans-serif";var a=e.createElement("script");a.setAttribute("class","zarplataWidgetContent"),a.setAttribute("type","text/javascript"),a.setAttribute("charset","UTF-8"),a.setAttribute("async",!0),a.setAttribute("src","https://vacancy-widget.zp.ru/current.js");var r=e.getElementsByClassName("zarplataWidgetLoader")[0];r.parentNode.insertBefore(a,r.nextSibling)}(window,document);</script>
		<div class="info-page__resume">
			<button data-fancybox data-src="#modal01" class="button"> Отправить свое резюме</button>
		</div>
	</div>
</div>      
<?php get_footer(); ?>
