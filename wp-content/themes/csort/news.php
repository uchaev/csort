<?php
/**
 * Template Name: Новости
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
	<div class="news__list">
		<?php 
			$offset = $_GET['cur'] != null ? $_GET['cur'] : 0;
			global $limit, $news_count, $page;
			$limit = 4;
			$news_page = ((int)($offset / $limit)) + 1;
			$args = array(
			   'post_type' => 'news',
			   'publish' => true,
			   'orderby' => 'date',
			   'order' => 'date',
			   'posts_per_page' => $limit,
			   'offset' => $offset,
			);
			$news_count = ((int)(wp_count_posts('news')->publish / $limit)) + 1;
			
		?>
		<?php $news = new WP_Query($args); while ($news->have_posts()) { $news->the_post(); ?>
			<div class="article news__list-item">
				<div class="article__images">
					<?php if (types_render_field('news_img',array('url'=>'true')) != NULL) {?>
						<img src="<?php echo types_render_field('news_img',array('url'=>'true'))?>" alt="<?php the_title();?>">
					<?php } else { ?>
						<img src="<?php bloginfo('template_url'); ?>/images/main/news-icon.gif" alt="<?php the_title();?>">
					<?php } ?>
				</div>
				<div class="article__info">
					<div class="article__info-top">
						<div class="article__info-top-image">
						<?php if (types_render_field('news_img',array('url'=>'true')) != NULL) {?>
							<img src="<?php echo types_render_field('news_img',array('url'=>'true'))?>" alt="<?php the_title();?>">
						<?php } else { ?>
							<img src="<?php bloginfo('template_url'); ?>/images/main/news-icon.gif" alt="<?php the_title();?>">
						<?php } ?>
							
						</div>
						<div class="article__info-top-desc">
							<div class="article__info-date"><span><?php echo the_date();?></span></div>
							<div class="article__info-title">
								<h2><a href="<?php the_permalink();?>"><i></i><?php the_title();?></a></h2>
							</div>
						</div>
					</div>
					<div class="article__info-bottom">
						<div class="article__info-desc">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php } wp_reset_postdata(); ?>
	<?php if ($news_count > 1) { ?>
	<div class="news__pagination">
		<div class="pagination">
			<?php for ($i = 1; $i <= $news_count; $i++) { ?>
				<?php if ($news_page == $i) { ?>
					<div class="pagination__item"><span><?php echo $i; ?></span></div>
				<?php } else { ?>
					<div class="pagination__item"><a href="<?php echo the_permalink().'?cur='.(($i - 1) * $limit);?>"><?php echo $i; ?></a></div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>	
	<?php } ?>
<?php get_footer(); ?>
