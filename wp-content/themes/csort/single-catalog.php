<?php
/**
 * Template Name: Линия оборудования
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
<div class="catalog">
    <div class="catalog__product">
        <?php
        $equipment_args = array(
            'post_type' => 'equipment',
            'numberposts' => -1,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key' => '_wpcf_belongs_catalog_id',
                    'value' => get_the_ID()
                )
            )
        );
        $index = 0;
        ?>
        <?php $equipments = new WP_Query($equipment_args);
        while ($equipments->have_posts()) {
            $equipments->the_post(); ?>
            <div class="modal" id="modal<?php echo $index; ?>" style="display: none;">
                <div class="modal__wrapper">
                    <div class="modal__header modal__header_price">
                        Уточнить цену на <?php the_title(); ?>
                    </div>
                    <div js-content-main class="modal__content">
                        <form js-form action="#">
                            <input name="form_type" type="hidden" value="equipment"/>
                            <input name="form_title" type="hidden" value="<?php the_title(); ?>"/>
                            <div js-form-row class="modal__row">
                                <div class="modal__grid modal__grid_label">
                                    <span> Ваше ФИО<sup>*</sup></span>
                                </div>
                                <div class="modal__grid modal__grid_input">
                                    <input name="personal" js-form-required type="text">
                                </div>
                            </div>
                            <div js-form-row class="modal__row">
                                <div class="modal__grid modal__grid_label">
                                    <span> E-mail<sup>*</sup></span>
                                </div>
                                <div class="modal__grid modal__grid_input">
                                    <input name="email" js-form-required type="text">
                                </div>
                            </div>
                            <div js-form-row class="modal__row">
                                <div class="modal__grid modal__grid_label">
                                    <span> Телефон <sup>*</sup> </span>
                                </div>
                                <div class="modal__grid modal__grid_input">
                                    <input name="phone" js-form-required type="text">
                                </div>
                            </div>
                            <div js-form-row class="modal__row modal__row_button">
                                <div class="modal__grid modal__grid_button">
                                    <button type="submit" class="button button_form">Отправить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div js-content-succes class="modal__content modal__content_hidden">
                        <div js-form-row class="modal__row">
                            <div class="modal__grid modal__grid_success">
                                <span> Спасибо за заявку</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal__footer">
                        <span> Оставьте Ваши координаты и мы свяжемся с Вами в ближайшее время </span>
                    </div>
                </div>
            </div>

            <div class="product-card">
                <div class="product-card__col product-card__col_left">
                    <div class="product-card__title product-card__title_mobile">
                        <div class="title title_regular title_color"><?php the_title(); ?></div>
                    </div>
                    <div class="product-card__image">
                        <a href="<?php echo types_render_field('equipment_img', array('url' => 'true')) ?>"
                           data-fancybox> <img
                                    src="<?php echo types_render_field('equipment_img', array('url' => 'true')) ?>"
                                    alt=""> </a>
                    </div>
                    <div class="product-card__button product-card__button_price">
                        <button class="button button_price" data-fancybox data-src="#modal<?php echo $index++; ?>"
                                href="javascript:;">
                            <div class="button__icon button__icon_price"></div>
                            <span>Уточнить цену </span>
                        </button>
                    </div>
                </div>
                <div class="product-card__col product-card__col_right">
                    <div class="product-card__info">
                        <div class="product-card__title">
                            <h2 class="title title_regular title_color"><?php the_title(); ?></h2>
                        </div>
                        <div class="product-card__wrapper">
                            <div class="product-card__desc">
                                <div class="text-content">
                                    <?php the_content(); ?>
                                </div>
                                <div class="product-card__button product-card__button_next">
                                    <a class="button button_next" href="<?php the_permalink(); ?>">
                                        <div class="button__wrapper"><span>Подробнее</span></div>
                                    </a>
                                </div>
                            </div>

                            <?php

                            if (types_render_field('equipment_file', array('url' => 'true')) !== '') { ?>
                            <div class="product-card__file">
                                <a target="_blank"
                                   href="<?php echo types_render_field('equipment_file', array('url' => 'true')) ?>"
                                   class="product-card__file-wrapper">
                                    <div class="product-card__file-image">
                                        <img src="<?php bloginfo('template_url'); ?>/images/main/pdf-icon.png"
                                             alt="Буклет <?php the_title(); ?>">
                                    </div>
                                    <div class="product-card__file-title">
                                        <span>Буклет <?php the_title(); ?></span>
                                    </div>
                                </a>
                                <?php }  else { } ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
        wp_reset_postdata(); ?>
        <?php if (types_render_field('catalog_show_recomended', array('row' => 'true')) == 1) { ?>
            <div class="catalog__recommend">
                <div class="recommend">
                    <div class="recommend__title">
                        С этим оборудованием также приобретают:
                    </div>
                    <div class="recommend__wrapper">
                        <?php
                        $recomended_args = array(
                            'post_type' => 'equipment',
                            'numberposts' => -1,
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'meta_query' => array(
                                array(
                                    'key' => 'wpcf-equipment_show_in_recomended',
                                    'value' => '1'
                                )
                            )
                        );
                        ?>
                        <?php $recomended = new WP_Query($recomended_args);
                        while ($recomended->have_posts()) {
                            $recomended->the_post(); ?>
                            <a href="<?php the_permalink(); ?>" class="recommend__item">
                                <div class="recommend__item-image">
                                    <img src="<?php echo types_render_field('equipment_img', array('url' => 'true')) ?>"
                                         alt="<?php the_title(); ?>">
                                </div>
                                <div class="recommend__item-info">
                                    <div class="recommend__item-title"><?php the_title(); ?></div>
                                    <div class="recommend__item-desc">
                                        <?php echo types_render_field('equipment_short_desc', array('row' => 'true')) ?>
                                    </div>
                                    <div class="recommend__item-link">
                                        <span><i></i>Подробнее</span>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php get_footer(); ?>
