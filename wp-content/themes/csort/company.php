<?php
/**
 * Template Name: О компании
 * @package csort
 * @subpackage csort
 */

get_header(); ?>  
  <div class="info-page">
		<div class="info-page__title"><h1 class="title title_large title_regular"><?php?></h1></div>
		<div class="info-page__wrapper">
			<div class="info-page__main">
				<div class="text-content">
					<?php the_content(); ?>
				</div>

				<div class="info-page__bonus">
					<div class="bonus catalog__bonus">
						<div class="bonus__item bonus__item_company">
							<div class="bonus__item-img">
								<img src="<?php bloginfo('template_url'); ?>/images/main/dealer-icon-grey.png" alt="  ">
							</div>
							<div class="bonus__item-info">
								<div class="bonus__item-title">
									ДИЛЕРЫ
								</div>
								<div class="bonus__item-desc">
									Продажа фотосепараторов "СиСорт" в различных странах мира возможна с
									участием дилеров компании. Обратитесь к нам, чтобы узнать, как
									связаться с
									дилером в Вашей стране.
								</div>
							</div>
						</div>
						<div class="bonus__item bonus__item_company">
							<div class="bonus__item-img">
								<img src="<?php bloginfo('template_url'); ?>/images/main/leasing-icon-grey.png" alt="  ">
							</div>
							<div class="bonus__item-info">
								<div class="bonus__item-title">
									ЛИЗИНГ
								</div>
								<div class="bonus__item-desc">
									Для Вашего удобства мы сотрудничаем с финансовыми организациями,
									предоставляющими лизинг для наших клиентов на выгодных условиях.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="info-page__aside">
				<div class="video-reviews">
					<div class="video-reviews__title"><span>Видео отзывы</span></div>
					<div js-video-reviews class="owl-carousel owl-theme video-reviews__slider">
						<?php 
							$args = array(
							   'post_type' => 'video_feedback',
							   'publish' => true,
							   'numberposts' => -1,
							   'orderby' => 'date',
							   'order' => 'DESC',
							);
						?>
						<?php $video = new WP_Query($args); while ($video->have_posts()) { $video->the_post(); ?>
							<a href="<?php echo types_render_field('video_feedback_url',array('raw'=>'true')); ?>" target="_blank" class="item video-reviews__slider-item">
								<div class="video-reviews__slider-image"
									 style="background-image: url('<?php echo types_render_field('video_feedback_img',array('url'=>'true'))?>')"></div>
								<div class="video-reviews__slider-desc">
									<?php the_title(); ?>
								</div>
							</a>
						<?php } wp_reset_postdata(); ?>
					</div>
					<div class="video-reviews__button">
						<a href="<?php echo get_option('video_feedback_input_setting'); ?>" target="_blank" class="button button_next">
							<div class="button__wrapper"><span> Посмотреть все отзывы</span></div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
