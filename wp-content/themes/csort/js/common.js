"use strict";

$(function () {

    // custom scrollbar
    $('[js-burger-siderbar]').mCustomScrollbar({
        axis: "y", // horizontal scrollbar
        theme: "dark"
    });

    // open sidebar
    $('[js-burger]').on('click', function () {
        $('[js-burger-siderbar]').addClass('header-mobile__sidebar_open');
        $('[js-header-mobile-middle]').addClass('header-mobile__middle_hide');
    });

    $('[js-close-mobile-sidebar]').on('click', function () {
        $('[js-burger-siderbar]').removeClass('header-mobile__sidebar_open');
        $('[js-header-mobile-middle]').removeClass('header-mobile__middle_hide');
    });
});
'use strict';

$(function () {
    var $button = $('[js-mobile-button]');
    $(window).scroll(function () {
        if ($button.isInView() == false) {
            $('[js-mobile-fixed-button]').addClass('mobile-fixed-button_active');
        } else {
            $('[js-mobile-fixed-button]').removeClass('mobile-fixed-button_active');
        }
    });
});
'use strict';

$(function () {
    var $video = $('[js-category-card-video]');
    var $videoTitle = $('[js-category-card-video-title]');
    calculate();

    $(window).resize(function () {
        calculate();
    });

    function calculate() {
        var videoWidth = $video.width();
        $videoTitle.width(videoWidth);
    }
});
'use strict';

$(function () {
    $('[js-slider-main]').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });

    $('[js-slider-mobile]').owlCarousel({
        loop: true,
        margin: 0,
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });
});
"use strict";

$(function () {});
"use strict";
"use strict";
'use strict';

$(function () {
    $('[js-form]').submit(function (e) {
        var $form = $(this);
        $form.find('[js-form-required]').each(function (e) {
            $(this).closest('[js-form-row]').removeClass('is-error');
            var valInput = $(this).val();
            if (!valInput) {
                $(this).closest('[js-form-row]').addClass('is-error');
            }
        });
        if ($form.find('.is-error').length !== 0) {
            e.preventDefault();
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: " /wp-admin/admin-ajax.php",
                data: $form.serialize() + "&pageLink=" + window.location + "&action=send_feedback"
            });

            $form.closest('.modal').find('[js-content-main]').addClass('modal__content_hidden');
            $form.closest('.modal').find('[js-content-succes]').removeClass('modal__content_hidden');

            setTimeout(function () {
                $.fancybox.close();
                setTimeout(function () {
                    $form.trigger('reset');
                    $form.closest('.modal').find('[js-content-main]').removeClass('modal__content_hidden');
                    $form.closest('.modal').find('[js-content-succes]').addClass('modal__content_hidden');
                }, 1500);
            }, 3000);
            return false;
        }
    });
});
'use strict';

$(function () {

    var $window = $(window),
        lastScrollTop = 0;

    var onScroll = function onScroll(e) {
        var top = $window.scrollTop();
        if (lastScrollTop > top) {
            $('.header-mobile__top').show();
        } else if (lastScrollTop < top) {
            $('.header-mobile__top').hide();
        }
        lastScrollTop = top;
    };

    $window.scroll(function () {
        if ($window.scrollTop() < 100) {
            $('.header-mobile__top').show();
            return false;
        }
        onScroll();
    });

    // $window.on('scroll', onScroll);
});
'use strict';

$(function () {
    var fixed = function fixed() {
        setTimeout(function () {
            if ($(window).width() <= 650) {
                $('.zarplataWidget-card__column').attr('style', 'float:none!important;width:100%!important');
                $('.zarplataWidget-card__text-right').attr('style', 'margin-top:20px!important');
            } else {
                $('.zarplataWidget-card__column').attr('style', '');
                $('.zarplataWidget-card__text-right').attr('style', '');
            }
        }, 1000);
    };
    fixed();
    $(window).resize(function () {
        fixed();
    });
});
'use strict';

$(function () {
    $(window).scroll(function () {
        review();
    });

    function review() {
        $('[js-mobile-review]').each(function () {
            var srcGif = $(this).data('src-gif');
            var srcImage = $(this).data('src-image');
            if ($(this).isInView()) {
                if ($(this).attr('src') != srcGif) {
                    $(this).attr('src', srcGif);
                }
            } else {
                $(this).attr('src', srcImage);
            }
        });
    }
});

/*
 */
'use strict';

$(document).ready(function () {
    $('[nice-select]').niceSelect();
});
'use strict';

$(function () {

    var $owl = $('[js-product-gallery-list]');

    $owl.owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            }
        }
    });

    $('[js-product-gallery-prev]').on('click', function () {
        $owl.trigger('prev.owl.carousel');
    });

    $('[js-product-gallery-next]').on('click', function () {
        $owl.trigger('next.owl.carousel');
    });
});
/*
$(function () {
    let $slider = $('[js-sort-product-cards]');

    let sliderInit = (slider) => {
            slider.addClass('owl-carousel owl-theme');
            slider.owlCarousel({
                loop: true,
                margin: 10px,
                nav: false,
                dots: false,
                center: true,
                responsive: {
                    0: {
                        items: 2
                    }
                }
            })
        },
        sliderDestroy = (slider) => {
            slider.owlCarousel('destroy');
            slider.removeClass('owl-carousel owl-theme owl-loaded')
        },
        sliderUpdate = (slider) => {
            if ($(window).width() <= 650) {
                sliderInit(slider);
            }
            else {
                sliderDestroy(slider)
            }
        };

    sliderUpdate($slider);
    $(window).resize(function () {
        sliderUpdate($slider)
    });

    $sliderControlPrev.on('click', function () {
        $slider.trigger('prev.owl.carousel')
    });

    $sliderControlNext.on('click', function () {
        $slider.trigger('next.owl.carousel')
    });

});*/
"use strict";
'use strict';

$(function () {
  $('[js-sort-product-show]').on('click', function () {
    $(this).next().toggleClass('sort-product__nav-list_visible');
  });
});
'use strict';

$(function () {
    $('[js-tab]').on('click', function (e) {
        e.preventDefault();
        $('[js-tab]').removeClass('product-tabs__header-item_active');

        $(this).addClass('product-tabs__header-item_active');
        var number = $(this).attr('js-tab');

        $('[js-tab-content]').removeClass('product-tabs__content_show');
        $('[js-tab-content]').each(function () {
            if ($(this).attr('js-tab-content') == number) {
                $(this).addClass('product-tabs__content_show');
            }
        });

        $('[js-product-tabs-mobile]').removeClass('product-tabs__mobile-header_show');
        $('[js-product-tabs-mobile]').each(function () {
            if ($(this).attr('js-product-tabs-mobile') == number) {
                $(this).addClass('product-tabs__mobile-header_show');
            }
        });
    });

    $('[js-product-tabs-mobile]').on('click', function (e) {

        var number = $(this).attr('js-product-tabs-mobile');

        e.preventDefault();

        $(this).toggleClass('product-tabs__mobile-header_show');
        $(this).next().toggleClass('product-tabs__content_show');
        $('[js-product-tabs-mobile]').toggleClass('product-tabs__mobile-header_show');

        /*  $('[js-tab-content]').each(function () {
              if ($(this).attr('js-tab-content') == number) {
                  $(this).addClass('product-tabs__content_show')
              }
          });*/

        /*$('[js-product-tabs-mobile]').each(function () {
            if ($(this).attr('js-product-tabs-mobile') == number) {
                $(this).addClass('product-tabs__mobile-header_show')
            }
        });
        */
        /*
                $('[js-tab]').removeClass('product-tabs__header-item_active');
        
                $('[js-tab]').each(function () {
                    if ($(this).attr('js-tab') == number) {
                        $(this).addClass('product-tabs__header-item_active')
                    }
                })*/
    });

    function closeTabs() {
        $('[js-product-tabs-mobile]').removeClass('product-tabs__mobile-header_show');
        $('[js-tab-content]').removeClass('product-tabs__content_show');
    }

    function openTabs() {
        $('[js-product-tabs-mobile="1"]').addClass('product-tabs__mobile-header_show');
        $('[js-tab-content="1"]').addClass('product-tabs__content_show');
        $('[js-tab]').removeClass('product-tabs__header-item_active"');
        $('[js-tab="1"]').removeClass('product-tabs__header-item_active"');
    }

    if ($(window).width() <= 650) {

        closeTabs();
    } else {
        openTabs();
    }

    $(window).resize(function () {
        if ($(window).width() <= 650) {
            closeTabs();
        } else {
            openTabs();
        }
    });
});
'use strict';

$(function () {

    $('[js-form-contact]').submit(function () {
        yaCounter12212590.reachGoal('kontakt');
    });
});
'use strict';

$(function () {

    var maxWidth = $('.content__wrapper').width();
    var asideWidth = $('.product__aside').outerWidth();
    var contentWidth = maxWidth - asideWidth - 50;

    $('.text-content_product table').each(function () {

        var $table = $(this);
        $table.wrap('<div class="table table_product" style="max-width:' + contentWidth + 'px"> </div>');
    });

    $('.text-content_product iframe').each(function () {
        var $iframe = $(this);
        $iframe.wrap('<div class="youtube youtube_product" style="max-width:' + contentWidth + 'px"> </div>');
    });

    $(window).resize(function () {
        if ($(window) <= 650) {
            return false;
        }
        var maxWidth = $('.content__wrapper').outerWidth();
        var asideWidth = $('.product__aside').outerWidth();
        var contentWidth = maxWidth - asideWidth - 50;

        $('.text-content_product .table_product').each(function () {
            var $table = $(this);
            $table.css('max-width', contentWidth);
        });

        $('.text-content_product .youtube_product').each(function () {
            var $iframe = $(this);
            $iframe.css('max-width', contentWidth);
        });
    });
});
'use strict';

$(function () {
    $(function () {
        $('[js-video-reviews]').owlCarousel({
            loop: true,
            margin: 0,
            autoplay: true,
            autoplayTimeout: 2500,
            autoplayHoverPause: true,
            dots: true,
            responsive: {
                0: {
                    items: 1
                }
            }
        });
    });
});
//# sourceMappingURL=common.js.map
