<?php
/**
 * Template Name: Оборудование
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
	<div class="modal" id="modal" style="display: none;">
		<div class="modal__wrapper">
			<div class="modal__header modal__header_price">
				Уточнить цену на <?php the_title();?>
			</div>
			<div js-content-main class="modal__content">
				<form js-form action="#">
					<input name="form_type" type="hidden" value="equipment"/>
					<input name="form_title" type="hidden" value="<?php the_title();?>"/>
					<div js-form-row class="modal__row">
						<div class="modal__grid modal__grid_label">
							<span> Ваше ФИО<sup>*</sup></span>
						</div>
						<div class="modal__grid modal__grid_input">
							<input name="product" type="text" hidden value="продукт">
							<input name="personal" js-form-required type="text">
						</div>
					</div>
					<div js-form-row class="modal__row">
						<div class="modal__grid modal__grid_label">
							<span> E-mail<sup>*</sup></span>
						</div>
						<div class="modal__grid modal__grid_input">
							<input name="email" js-form-required type="text">
						</div>
					</div>
					<div js-form-row class="modal__row">
						<div class="modal__grid modal__grid_label">
							<span> Телефон <sup>*</sup> </span>
						</div>
						<div class="modal__grid modal__grid_input">
							<input name="phone" js-form-required type="text">
						</div>
					</div>
					<div js-form-row class="modal__row modal__row_button">
						<div class="modal__grid modal__grid_button">
							<button type="submit" class="button button_form">Отправить</button>
						</div>
					</div>
				</form>
			</div>
			<div js-content-succes class="modal__content modal__content_hidden">
				<div js-form-row class="modal__row">
					<div class="modal__grid modal__grid_success">
						<span> Спасибо за заявку</span>
					</div>
				</div>
			</div>
			<div class="modal__footer">
				<span> Оставьте Ваши координаты и мы свяжемся с Вами в ближайшее время </span>
			</div>
		</div>
	</div>
    <div class="modal" id="modal-discount" style="display: none;">
    <div class="modal__wrapper">
        <div class="modal__header modal__header_price">
            Информация по акции - 15 %
        </div>
        <div js-content-main class="modal__content">
            <form js-form action="#">
                <input name="form_type" type="hidden" value="equipment"/>
                <input name="form_title" type="hidden" value="<?php the_title();?>"/>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Ваше ФИО<sup>*</sup></span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="product" type="text" hidden value="продукт">
                        <input name="personal" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> E-mail<sup>*</sup></span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="email" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Телефон <sup>*</sup> </span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="phone" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row modal__row_button">
                    <div class="modal__grid modal__grid_button">
                        <button type="submit" class="button button_form">Получить</button>
                    </div>
                </div>
            </form>
        </div>
        <div js-content-succes class="modal__content modal__content_hidden">
            <div js-form-row class="modal__row">
                <div class="modal__grid modal__grid_success">
                    <span> Спасибо за заявку</span>
                </div>
            </div>
        </div>
        <div class="modal__footer">
            <span> Оставьте Ваши координаты и мы свяжемся с Вами в ближайшее время </span>
        </div>
    </div>
</div>
	<div class="product">
		<div class="product__wrapper">
			<div class="product__aside">
				<div class="product__title product__title_mobile">
					<span class="title title_color title_big title_regular"><?php the_title();?></span>
				</div>
				<div class="product__section product__section_price product__section_mobile">
					<button js-mobile-button class="button button_price" data-fancybox data-src="#modal">
						<div class="button__icon button__icon_price"></div>
						<span>Уточнить цену </span>
					</button>
				</div>
				<div class="product__section product__section_gallery">
					<div class="product-gallery">
						<div js-product-gallery-list class="owl-carousel owl-theme">
							<div class="product-gallery__item">
								<a href="<?php echo types_render_field('equipment_img',array('url'=>'true'))?>" data-fancybox="product">
									<img src="<?php echo types_render_field('equipment_img',array('url'=>'true'))?>" alt="<?php the_title();?>">
								</a>
							</div>
							<?php if (types_render_field('equipment_img2',array('url'=>'true')) != NULL) { ?>
							<div class="product-gallery__item">
								<a href="<?php echo types_render_field('equipment_img2',array('url'=>'true'))?>" data-fancybox="product">
									<img src="<?php echo types_render_field('equipment_img2',array('url'=>'true'))?>" alt="">
								</a>
							</div>
							<?php } ?>
							<?php if (types_render_field('equipment_img3',array('url'=>'true')) != NULL) { ?>
							<div class="product-gallery__item">
								<a href="<?php echo types_render_field('equipment_img3',array('url'=>'true'))?>" data-fancybox="product">
									<img src="<?php echo types_render_field('equipment_img3',array('url'=>'true'))?>" alt="">
								</a>
							</div>
							<?php } ?>
							<?php if (types_render_field('equipment_img4',array('url'=>'true')) != NULL) { ?>
								<div class="product-gallery__item">
									<a href="<?php echo types_render_field('equipment_img4',array('url'=>'true'))?>" data-fancybox="product">
										<img src="<?php echo types_render_field('equipment_img4',array('url'=>'true'))?>" alt="">
									</a>
								</div>
							<?php } ?>
							<?php if (types_render_field('equipment_img5',array('url'=>'true')) != NULL) { ?>
								<div class="product-gallery__item">
									<a href="<?php echo types_render_field('equipment_img5',array('url'=>'true'))?>" data-fancybox="product">
										<img src="<?php echo types_render_field('equipment_img5',array('url'=>'true'))?>" alt="">
									</a>
								</div>
							<?php } ?>
							<?php if (types_render_field('equipment_img6',array('url'=>'true')) != NULL) { ?>
								<div class="product-gallery__item">
									<a href="<?php echo types_render_field('equipment_img6',array('url'=>'true'))?>" data-fancybox="product">
										<img src="<?php echo types_render_field('equipment_img6',array('url'=>'true'))?>" alt="">
									</a>
								</div>
							<?php } ?>
						</div>
						<?php if (types_render_field('equipment_img2',array('url'=>'true')) != NULL || types_render_field('equipment_img3',array('url'=>'true')) != NULL) { ?>
						<div class="product-gallery__nav">
							<div js-product-gallery-prev class="product-gallery__nav-next"></div>
							<div class="product-gallery__nav-middle"></div>
							<div js-product-gallery-next class="product-gallery__nav-prev"></div>
						</div>
						<?php } ?>
					</div>
				</div>

				<div class="product__section product__section_price product__section_hidden">
					<button class="button button_price" data-fancybox data-src="#modal"
							href="javascript:;">
						<div class="button__icon button__icon_price"></div>
						<span>Уточнить цену</span>
					</button>
				</div>

				<div class="product__section product__section_price">


				<?php  if (types_render_field('equipment_file',array('url'=>'true')) !== '' )   {?>
                    <div class="product__file">
                        <div class="product__file-title"><span>Рекомендуем ознакомиться</span></div>
                        <div class="product__file-wrapper">
                            <div class="product__file-icon">
                                <img src="<?php bloginfo('template_url'); ?>/images/main/pdf-icon.png" alt="#">
                            </div>
                            <div class="product__file-link">
                                <a target="_blank" href="<?php echo types_render_field('equipment_file',array('url'=>'true'))?>">Буклет <?php the_title();?></a>
                            </div>
                        </div>
                    </div>
                    <?php } else {} ?>
				</div>

			</div>
			<div class="product__main">
				<div class="product__title">
					<h1 class="title title_color title_big title_regular"><?php the_title();?></h1>

				</div>
                <div class="product-discount product__discount">
                    <div class="product-discount__info">
                        <b> Получи скидку 15 % </b>
                        <br>
                        на <?php the_title();?>
                    </div>
                    <div class="product-discount__action">
                        <button data-fancybox="" data-src="#modal-discount" class="button"><span>ПОЛУЧИТЬ</span></button>
                    </div>
                </div>
				<div class="product-tabs">
					<div class="product-tabs__header">
						<div js-tab="1"
							 class="product-tabs__header-item product-tabs__header-item_active product-tabs__header-item_desc">
							Описание
						</div>
						<div js-tab="2"
							 class="product-tabs__header-item  product-tabs__header-item_options">
							Параметры
						</div>
						<div js-tab="3"
							 class="product-tabs__header-item product-tabs__header-item_video">
							<div class="product-tabs__video">
								<div class="product-tabs__video-title">Видео</div>
							</div>
						</div>
					</div>
					<div class="product-tabs__main">
						<div class="product-tabs__item">
							<div js-product-tabs-mobile="1"
								 class="product-tabs__mobile-header product-tabs__mobile-header_desc product-tabs__mobile-header_show">
								<div class="product-tabs__mobile-title"> Описание</div>
								<div class="product-tabs__mobile-open"></div>
							</div>
							<div js-tab-content="1"
								 class="product-tabs__content product-tabs__content_show">
								<div class="text-content text-content_product">
									<?php echo types_render_field('equipment_desc',array('row'=>'true'))?>
								</div>
							</div>
						</div>
						<div class="product-tabs__item">
							<div js-product-tabs-mobile="2"
								 class="product-tabs__mobile-header product-tabs__mobile-header_options">
								<div class="product-tabs__mobile-title">Параметры</div>
								<div class="product-tabs__mobile-open"></div>
							</div>
							<div js-tab-content="2" class="product-tabs__content">
								<div class="text-content text-content_product">
									<?php echo types_render_field('equipment_params',array('row'=>'true'))?>
								</div>
							</div>
						</div>
						<div class="product-tabs__item product-tabs__item_video">
							<div js-product-tabs-mobile="3"
								 class="product-tabs__mobile-header product-tabs__mobile-header_video">
								<div class="product-tabs__mobile-title">
									<div class="product-tabs__video product-tabs__video_mobile">
										<div class="product-tabs__video-title product-tabs__video-title_mobile">
											отзывы
										</div>
									</div>
								</div>
								<div class="product-tabs__mobile-open"></div>
							</div>
							<div js-tab-content="3"
								 class="product-tabs__content product-tabs__content_video">
								<div class="text-content text-content_product">
									<?php echo types_render_field('equipment_video',array('row'=>'true'))?>
								</div>
							</div>
						</div>
					</div>
                    <?php
                    $ids = get_field('video_feedback_relative', false, false);
                    if ($ids) {
                        $relatives = new WP_Query(array(
                            'post_type' => 'video_feedback',
                            'posts_per_page' => -1,
                            'post__in' => $ids,
                            'post_status' => 'any',
                            'orderby' => 'post__in',
                        ));
                    } else {
                        $relatives = NULL;
                    }
                    ?>
                    <?php if ($relatives != NULL && $relatives->have_posts()) { ?>
					<div class="mobile-review">
						<div class="mobile-review__title">
							<span>Видео отзывы</span>
						</div>
						<div class="mobile-review__list">
                            <?php while ($relatives->have_posts()) { $relatives->the_post(); ?>
                                <div class="mobile-review-item mobile-review__list-item">
                                    <div class="mobile-review-item__content">
                                        <div class="mobile-review-item__content-title"><span><?php the_title();?></span></div>
                                        <div class="mobile-review-item__content-image">
                                            <a data-fancybox="" data-src="<?php echo types_render_field('video_feedback_url',array('raw'=>'true'))?>" href="#">
                                                <img js-mobile-review="" src="<?php echo types_render_field('video_feedback_gif',array('raw'=>'true'))?>"
                                                     data-src-image="<?php echo types_render_field('video_feedback_img',array('raw'=>'true'))?>"
                                                     data-src-gif="<?php echo types_render_field('video_feedback_gif',array('raw'=>'true'))?>" alt="">
                                            </a>
                                        </div>
                                        <div class="mobile-review-item__content-desc"><span><?php echo types_render_field('video_feedback_desc',array('raw'=>'true'))?></span></div>
                                    </div>
                                </div>
                            <?php } wp_reset_postdata(); ?>
						</div>
					</div>
                    <?php } ?>
				</div>

			</div>
		</div>
		<?php if(types_render_field('equipment_show_recomended',array('row'=>'true')) == 1) {?>
			<div class="product__recommend">
				<div class="recommend">
					<div class="recommend__title">
						С этим оборудованием также приобретают:
					</div>
					<div class="recommend__wrapper">
						<?php
							$recomended_args = array(
								'post_type' => 'equipment',
								'numberposts' => -1,
								'orderby' => 'date',
								'order' => 'DESC',
								'meta_query' => array(
									array(
										'key' => 'wpcf-equipment_show_in_recomended',
										'value' => '1'
									)
								)
							);
						?>
						<?php $recomended = new WP_Query($recomended_args); while ($recomended->have_posts()) { $recomended->the_post(); ?>
							<a href="<?php the_permalink();?>" class="recommend__item">
								<div class="recommend__item-image">
									<img src="<?php echo types_render_field('equipment_img',array('url'=>'true'))?>" alt="<?php the_title();?>">
								</div>
								<div class="recommend__item-info">
									<div class="recommend__item-title"><?php the_title();?></div>
									<div class="recommend__item-link">
										<span><i></i>Подробнее</span>
									</div>
								</div>
							</a>
						<?php } wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>

<?php get_footer(); ?>
<div js-mobile-fixed-button class="mobile-fixed-button ">
    <div class="mobile-fixed-button__item">
        <button data-fancybox data-src="#modal" class="button button_mobile-fixed"> <span> Уточнить цену </span></button>
    </div>
</div>

