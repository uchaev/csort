<?php
/**
 * Template Name: Новость
 * @package csort
 * @subpackage csort
 */

get_header();?>
<div class="info-page">
	<div class="info-page__title"><h1 class="title title_color title_large title_regular"><?php the_title();?></h1></div>
	<div class="info-page__wrapper">
		<div class="info-page__main">
			<div class="text-content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
