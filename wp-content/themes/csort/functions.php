<?php 
	//remove_filter('the_content', 'wpautop');
	@ini_set( 'upload_max_size' , '64M' );
	@ini_set( 'post_max_size', '64M');
	@ini_set( 'max_execution_time', '300' );

	function enqueue_styles() {
        wp_enqueue_style('vendor-style', get_stylesheet_directory_uri().'/css/vendor.css');
		wp_enqueue_style('common-style', get_stylesheet_directory_uri().'/css/common.css');

	}
	
	add_action('wp_enqueue_scripts', 'enqueue_styles');
	
	function enqueue_scripts() {
        wp_enqueue_script('vendor-script', get_template_directory_uri() . '/js/vendor.js');
		wp_enqueue_script('common-script', get_template_directory_uri() . '/js/common.js');

	}
	
	add_action('wp_enqueue_scripts', 'enqueue_scripts');
	
	function promresurs_register( $wp_customize ) {
        $wp_customize->add_section(
            'data_site_section',
            array(
                'title' => 'Контактная информация',
                'capability' => 'edit_theme_options',
                'description' => ''
            )
        );


        $wp_customize->add_setting(
            'phone_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'phone_input_control',
            array(
                'type' => 'text',
                'label' => "Номер телефона",
                'section' => 'data_site_section',
                'settings' => 'phone_input_setting'
            )
        );
		
		$wp_customize->add_setting(
            'additional_phone_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'additional_phone_input_control',
            array(
                'type' => 'text',
                'label' => "Дополнительный номер телефона",
                'section' => 'data_site_section',
                'settings' => 'additional_phone_input_setting'
            )
        );
		
		$wp_customize->add_setting(
            'facebook_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'facebook_input_control',
            array(
                'type' => 'text',
                'label' => "Facebook",
                'section' => 'data_site_section',
                'settings' => 'facebook_input_setting'
            )
        );
		
		$wp_customize->add_setting(
            'vk_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'vk_input_control',
            array(
                'type' => 'text',
                'label' => "VK",
                'section' => 'data_site_section',
                'settings' => 'vk_input_setting'
            )
        );
		
		$wp_customize->add_setting(
            'twitter_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'twitter_input_control',
            array(
                'type' => 'text',
                'label' => "Twitter",
                'section' => 'data_site_section',
                'settings' => 'twitter_input_setting'
            )
        );
		
		$wp_customize->add_setting(
            'youtube_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'youtube_input_control',
            array(
                'type' => 'text',
                'label' => "Youtube",
                'section' => 'data_site_section',
                'settings' => 'youtube_input_setting'
            )
        );
		
		$wp_customize->add_setting(
            'linkedin_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'linkedin_input_control',
            array(
                'type' => 'text',
                'label' => "Linkedin",
                'section' => 'data_site_section',
                'settings' => 'linkedin_input_setting'
            )
        );
		
		$wp_customize->add_setting(
            'video_feedback_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'video_feedback_input_control',
            array(
                'type' => 'text',
                'label' => "Видео отзывы",
                'section' => 'data_site_section',
                'settings' => 'video_feedback_input_setting'
            )
        );

        $wp_customize->add_setting(
            'feedback_email_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'feedback_email_input_control',
            array(
                'type' => 'text',
                'label' => "Отпавлять заявки на E-Mail",
                'section' => 'data_site_section',
                'settings' => 'feedback_email_input_setting'
            )
        );

        $wp_customize->add_setting(
            'sender_email_input_setting',
            array(
                'default' => '',
                'type' => 'option'
            )
        );
        $wp_customize->add_control(
            'sender_email_input_control',
            array(
                'type' => 'text',
                'label' => "Отпавлять заявки с E-Mail",
                'section' => 'data_site_section',
                'settings' => 'sender_email_input_setting'
            )
        );
    }
	add_action( 'customize_register', 'promresurs_register' );
	
	function get_file_name($file_name) {
		return pathinfo($file_name, PATHINFO_FILENAME);
	}
	
	add_action( 'get_file_name', 'get_file_name' );
	
	function get_menu_map($items) {
		if ($items == null || count($items) == 0) {
			return array();
		}
		$result_map = array();
		$count = count($items);
		
		for ($i = 0; $i < $count; $i++) {
			if ($items[$i]->menu_item_parent == 0) {
				$result_map[$items[$i]->ID] = array();
			}
		}
		for ($i = 0; $i < $count; $i++) {
			if ($items[$i]->menu_item_parent != 0) {
				$result_map[$items[$i]->menu_item_parent][]=$items[$i];
			}
		}		
		return $result_map;
	}
	add_action( 'get_menu_map', 'get_menu_map' );
	
	function get_active_menu_item($items, $object_id) {
		$active['active'] = current(wp_filter_object_list( $items, array( 'object_id' => $object_id)));
		$active['parent'] = $active['active']->menu_item_parent;
		return $active;
	}
	
	add_action( 'get_active_menu_item', 'get_active_menu_item' );
	
	function get_parent_menu_item($items, $parent_id) {
		$items_size = count($items);
		for ($i = 0; $i < $items_size; $i++) {
			if ($items[$i]->ID == $parent_id) {
				return $items[$i];
			}
		}
		return null;
	}
	
	add_action( 'get_parent_menu_item', 'get_parent_menu_item' );
	
	function get_menu_item_by_object_id($items, $object_id) {
		$items_size = count($items);
		for ($i = 0; $i < $items_size; $i++) {
			if ($items[$i]->object_id == $object_id) {
				return $items[$i];
			}
		}
		return null;
	}
	
	add_action( 'get_menu_item_by_object_id', 'get_menu_item_by_object_id' );
	
	
	function send_feedback() {
	    define('EQUIPMENT', 'equipment'); //уточнить цену
        define('CALLBACK', 'callback'); //обратный звонок (доп поле: удобное время time)
        define('TRIAL', 'trial'); //  пробная сортировка
        define('CONSULTATION', 'consultation'); //заявка на консультацию (доп поле: тема консультации consultation)
        define('JOB', 'job'); // работа в компании

		$fio = $_POST['personal'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$comment = $_POST['comment'];
		$time = $_POST['time'];
        $theme = $_POST['consultation'];
        $page = $_POST['pageLink'];

		$form_type = $_POST['form_type'];
        $form_title = $_POST['form_title'];

        $send_to_email = get_option('feedback_email_input_setting');
        $send_from_email = get_option('sender_email_input_setting');
        if ($send_to_email != null && $send_from_email != null) {
            $headers = "From: CSort <$send_from_email>\r\n";
            $message = "ФИО: $fio\r\nE-Mail: $email\r\nНомер телефона: $phone\r\nСообщение: $comment\r\n";
            if ($form_type == CALLBACK) {
                $message = $message."Удобное время: $time\r\n";
            } else if ($form_type == CONSULTATION) {
                $message = $message."Тема консультации: $theme\r\n";
            }
            $message = $message."Страница: $page\r\n";

            if(wp_mail($send_to_email, $form_title, $message, $headers)) {
                echo 'success';
            } else {
                echo 'error';
            }
        }
	}
	
	add_action('wp_ajax_send_feedback', 'send_feedback');
	add_action('wp_ajax_nopriv_send_feedback', 'send_feedback');
    add_action('get_header', 'remove_admin_login_header');

function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

