<?php
/**
 * The template for displaying the header
 *
 * @package WordPress
 * @subpackage csort
 * @since csort 1.0
 */
?>
<!DOCTYPE html>
<html class="">
    <head>
	    <meta charset="utf-8">
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
        <meta name="format-detection" content="telephone=no">
        <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title><?php the_title(); echo ' ‹ '.get_bloginfo('name'); ?></title>
		<?php wp_head(); ?>
    </head>
    <body class="body">
        <div class="body__wrapper" >
            <div class="header block">
				<div class="header-top">
					<div class="header-top__wrapper block__wrapper">
						<div class="header-top__col header-top__col_left">
							<div class="header-top__logo">
								<a class="header-top__logo-link" href="/" title="Csort"> 
									<img class="header-top__logo-image" src="<?php bloginfo('template_url'); ?>/images/main/logo.jpg" alt="Csort">
								</a>
							</div>
							<div class="header-top__desc">
								<div class="header-top__desc-text"> Компания <b> CSort </b> - производитель и
									<br>
									официальный партнер ведущих мировых
									<br>
									производителей фотосепараторов
								</div>
							</div>
						</div>
						<div class="header-top__col header-top__col_right">
							<div class="header-top__info">
								<div class="header-top__phone header-top__phone_main ">
									<div class="header-top__phone-number">
										<a class="header-top__phone-link" href="tel:<?php echo get_option('phone_input_setting'); ?>" title="<?php echo get_option('phone_input_setting'); ?>">
											<?php echo get_option('phone_input_setting'); ?> 
										</a>
									</div>
									<div class="header-top__phone-desc">
										Звонок по России бесплатный
									</div>
								</div>
								<div class="header-top__phone header-top__phone_additional ">
									<div class="header-top__phone-number">
										<a class="header-top__phone-link" href="tel:<?php echo get_option('additional_phone_input_setting'); ?>" title="<?php echo get_option('additional_phone_input_setting'); ?>">
											<?php echo get_option('additional_phone_input_setting'); ?>
										</a>
									</div>
									<div class="header-top__phone-desc">
									</div>
								</div>
								<div class="header-top__language">
									<div class="header-top__language-item">
										<a target="_blank" class="header-top__language-link" href="#" title="rus"> <img
													class="header-top__language-image" src="<?php bloginfo('template_url'); ?>/images/main/ru.png" alt="rus"></a>
									</div>
									<div class="header-top__language-item">
										<a target="_blank" class="header-top__language-link" href="#" title="eng"> <img
													class="header-top__language-image" src="<?php bloginfo('template_url'); ?>/images/main/en.png" alt="eng"></a>
									</div>
								</div>
							</div>
	<div class="header-top__desc header-top__desc_mobile">
								<div class="header-top__desc-text"> Компания <b> CSort </b> - производитель и
									<br>
									официальный партнер ведущих мировых
									<br>
									производителей фотосепараторов
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header-bottom">
					<div class="header-bottom__wrapper block__wrapper">
						<div class="navigation">
							<div class="navigation__list">
								<?php 
									global $items, $items_size, $active, $items_map;
									$items = wp_get_nav_menu_items(2);
									$items_size = count($items);
									$id = get_queried_object_id();
									$catalog_id = get_post_meta(get_the_ID(), '_wpcf_belongs_catalog_id');
									if ($catalog_id != NULL) {
										$id = $catalog_id[0];
									}
									$active = get_active_menu_item($items, $id);
									//news
									$news_object_id = 72;
									$news_id = 77;
									$nesw = strpos(get_permalink(), '/news/');
									if ($news != false) {
										$id = $news_object_id;
										$active['active'] = get_menu_item_by_object_id($items, $news_object_id);
										$active['parent'] = $news_id;
									}
									//articles
									$parent_id = 16;
									$article_object_id = 134;
									$articles = strpos(get_permalink(), '/article/');
									if ($articles != false) {
										$id = $article_object_id;
										$active['active'] = get_menu_item_by_object_id($items, $article_object_id);
										$active['parent'] = $parent_id;
									}
									//sorts 
									$parent_id = 205;
									$sorts_object_id = 189;
									$sorts = strpos(get_permalink(), '/sorts/');
									if ($sorts != false) {
										$id = $sorts_object_id;
										$active['active'] = get_menu_item_by_object_id($items, $sorts_object_id);
										$active['parent'] = $parent_id;
									}
									$items_map = get_menu_map($items);
								?>					
								<?php for($i = 0; $i < $items_size; $i++) { ?>
									<?php if ($items[$i]->menu_item_parent == 0) { ?>
									<a href="<?php echo $items[$i]->url?>" class="navigation__list-item <?php if ($active['active']->ID == $items[$i]->ID || $active['parent'] == $items[$i]->ID) {echo ' navigation__list-item_active';}?>" 
										title="<?php echo $items[$i]->title?>">
										<span title=""><?php echo $items[$i]->title?></span>
									</a>
									<?php } ?>
								<?php } wp_reset_postdata();?>
							</div>
						</div>
					</div>
				</div>
				<div class="header-mobile">
					<div class="header-mobile__top">
						<div class="header-mobile__top-col header-mobile__top-col_left">
							  <div class="header-mobile__language">
                  <div class="header-mobile__language"><a href="#" target="_blank" class="header-mobile__language-link"> En</a></div>
                </div>
						</div>
						<div class="header-mobile__top-col header-mobile__top-col_right">
							<div class="header-mobile__phone">
								<a href="tel:<?php echo get_option('phone_input_setting'); ?>" class="header-mobile__phone-link"><?php echo get_option('phone_input_setting'); ?></a>
								<div class="header-mobile__phone-desc">
									Звонок по РФ бесплатный
								</div>
							</div>
						</div>
					</div>
					<div js-header-mobile-middle class="header-mobile__middle">
						<div class="header-mobile__middle__col header-mobile-middle__col_left">
							<a href="" class="header-mobile__logo-link"> <img class="header-mobile__logo-image"
																			  src="<?php bloginfo('template_url'); ?>/images/main/logo-mobile.png" alt=""> </a>
						</div>
						<div class="header-mobile__middle__col header-mobile__middle-col_middle">
							<div class="header-mobile__title">

							</div>
						</div>
						<div js-burger
							 class="header-mobile__middle__col header-mobile__middle-col_right">
							<div class="header-mobile__burger">
								<div class="header-mobile__burger-item"></div>
							</div>
						</div>
					</div>
					<div js-burger-siderbar class="header-mobile__sidebar">
						<div class="header-mobile__sidebar-section header-mobile__sidebar-section_title">
							<div class="header-mobile__sidebar-img">

							</div>
							<div js-close-mobile-sidebar class="header-mobile__sidebar-close">

							</div>
						</div>
						<div class="header-mobile__sidebar-section header-mobile__sidebar-section_menu">
							<div class="navigation-mobile">
								<?php 
									$first = true;
									for($i = 0; $i < $items_size; $i++) { ?>
									<?php if ($items[$i]->menu_item_parent == 0) {?>
										<?php if ($first) { ?>
											<div class="navigation-mobile__list">
												<a href="<?php echo $items[$i]->url?>" class="navigation-mobile__list-item navigation-mobile__list-item_active"> 
													<span><?php echo $items[$i]->title?></span>
												</a>
											</div>
											<?php $first = false; ?>
										<?php } else { ?>
											<div class="navigation-mobile__wrapper">
												<a href="<?php echo $items[$i]->url?>" class="navigation-mobile__list-item">
													<span><?php echo $items[$i]->title?></span>
												</a>
											</div>
										<?php } ?>
									<?php } ?>
								<?php }  wp_reset_postdata();?>
							</div>
						</div>
						<div class="header-mobile__sidebar-section">
						</div>
					</div>
				</div>
			</div>
			<div class="content block">
				<div class="content__wrapper block__wrapper">
					<div class="content__main">
						<div class="content__section content__section_catalog">
							<div class="main">
								<?php if (!strpos(get_permalink(), '/contact') && !is_front_page()) { ?>
								<div class="breadcrumbs">
									<ul>
										<?php
											$parent = get_parent_menu_item($items, $active['parent']);
											if ($parent != NULL) {
												$sub_items = $items_map[$parent->ID];
											} else {
												$id = get_the_ID();
												$item = get_menu_item_by_object_id($items, $id);
												if ($item != NULL) {
													$id = $item->ID;
												}
												$sub_items = $items_map[$id];
											}
											$sub_size = count($sub_items);
										?>
										<?php if ($parent != NULL) {?>
											<li>
												<a href="<?php echo $parent->url ?>" class="<?php if ($parent->ID == $active['active']->ID) {echo 'is-active';}?>"> 
													<?php if ($parent->ID == $active['active']->ID) {echo '<i></i>';}?>
													<?php echo get_the_title($parent->object_id); ?>
												</a>
											</li>
										<?php } else { ?>
											<li>
												<a href="<?php echo get_permalink(); ?>" class="is-active"> 
													<i></i><?php echo the_title(); ?>
												</a>
											</li>
										<?php } ?>
										
										<?php if ($sub_items != NULL) { ?>
											<?php for ($i = 0; $i < $sub_size; $i++) {?>
												<li>
													<a href="<?php echo $sub_items[$i]->url?>" class="<?php if ($sub_items[$i]->ID == $active['active']->ID) {echo 'is-active';}?>">
														<?php 
															if ($sub_items[$i]->ID == $active['active']->ID) { 
																echo '<i></i>';
															}
															echo $sub_items[$i]->title;
														?>
													</a>
												</li>
											<?php } ?>
										<?php } wp_reset_postdata(); ?>
									</ul>
								</div>
								<?php } ?>