<?php
/**
 * Template Name: Сортируемые продукты
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
	<div class="modal" id="modal01" style="display: none;">
		<div class="modal__wrapper">
			<div class="modal__header modal__header_price">
				Запись на тестовую сортировку
			</div>
			<div js-content-main class="modal__content">
				<form js-form action="#">
					<input name="form_type" type="hidden" value="trial"/>
					<input name="form_title" type="hidden" value="Запись на тестовую сортировку"/>
					<div js-form-row class="modal__row">
						<div class="modal__grid modal__grid_label">
							<span> Ваше ФИО</span>
						</div>
						<div class="modal__grid modal__grid_input">
							<input name="product" type="text" hidden value="продукт">
							<input name="personal" type="text">
						</div>
					</div>
					<div js-form-row class="modal__row">
						<div class="modal__grid modal__grid_label">
							<span> Телефон <sup>*</sup> </span>
						</div>
						<div class="modal__grid modal__grid_input">
							<input name="phone" js-form-required type="text">
						</div>
					</div>
					<div js-form-row class="modal__row modal__row_button">
						<div class="modal__grid modal__grid_button">
							<button type="submit" class="button button_form">Отправить</button>
						</div>
					</div>
				</form>
			</div>
			<div js-content-succes class="modal__content modal__content_hidden">
				<div js-form-row class="modal__row">
					<div class="modal__grid modal__grid_success">
						<span> Спасибо за заявку</span>
					</div>
				</div>
			</div>
			<div class="modal__footer">
				<span> Мы уверены в высокой точности работы фотосепараторов и готовы продемонстрировать её Вам. Отправьте нам Ваш неочищенный продукт, и мы проведем тестовую сортировку на выбранном Вами фотосепараторе. Таким образом, Вы получите возможность оценить эффективность фотосортировки именно Вашего продукта.</span>
			</div>
		</div>
	</div>
	<div class="sort-product">
        <div class="sort-product__title">
            <h1 class="title title title_large title_color"><?php the_title();?></h1>
        </div>
		<div class="sort-product__desc">
			<div class="text-content">
				<?php the_content();?>
			</div>
		</div>
		<div class="sort-product__button">
			<button data-fancybox data-src="#modal01"
					class="button button_price button_price-mobile">
				<span class="button__icon button__icon_trial button__icon_trial-mobile"></span>
				<span>Заказать на тестовую сортировку</span>
			</button>
		</div>
        <!--_-->
        <div class="sort-product__wrapper sort-product__wrapper_intro">
            <div class="sort-product__aside sort-product__aside_color">
                <?php
                    $args = array(
                        'post_type' => 'sort_types',
                        'numberposts' => 500,
                        'orderby' => 'date',
                        'order' => 'ASC',
                    );
                    $types = new WP_Query($args);
                ?>
                <?php while($types->have_posts()) { $types->the_post(); $img_url = types_render_field('sort_types_img',array('url'=>'true'));?>
                <div class="sort-product__nav ">
                    <div class="sort-product__nav-title">
                        <?php the_title();?>
                    </div>
                    <?php
                        $args_sorts = array(
                            'post_type' => 'sorts',
                            'numberposts' => 500,
                            'orderby' => 'date',
                            'order' => 'ASC',
                            'meta_query' => array(
                                array(
                                    'key' => '_wpcf_belongs_sort_types_id',
                                    'value' => get_the_ID()
                                )
                            )
                        );
                        $type = get_post(); $sorts = new WP_Query($args_sorts);
                        $title = get_the_title();
                    ?>
                    <?php if ($types) { ?>
                    <div class="sort-product__nav-list">
                        <div class="sort-product__nav-item">
                            <?php while ($sorts->have_posts()) { $sorts->the_post(); ?>
                                <div class="sort-product__nav-link">
                                    <a href="<?php the_permalink();?>"><?php echo types_render_field('sorts_name',array('raw'=>'true'))?></a>
                                </div>
                            <?php } $sorts->reset_postdata(); setup_postdata($type); ?>
                                <div class="sort-product-mobile sort-product__nav-mobile">
                                    <div class="sort-product-mobile__title">
                                        <div class="sort-product-mobile__title-left">
                                            <?php echo $title; ?>
                                        </div>
                                    </div>
                                    <div class="sort-product-mobile__list">
                                        <?php while ($sorts->have_posts()) { $sorts->the_post(); ?>
                                        <a href="<?php the_permalink()?>"  class="sort-product-mobile__item">
                                            <div class="sort-product-mobile__item-image"
                                                 style="background-image: url('<?php echo types_render_field('sorts_img1',array('url'=>'true'))?>')">
                                                <div class="sort-product-mobile__item-label"><?php echo types_render_field('sorts_name',array('raw'=>'true'))?></div>
                                            </div>
                                        </a>
                                        <?php } $sorts->reset_postdata(); setup_postdata($type); ?>
                                    </div>
                                </div>

                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php } wp_reset_postdata();?>
            </div>
            <div class="sort-product__main sort-product__main_hidden-mobile sort-product__main_intro">
                <div class="sort-product__intro">
                    <div class="text-content">
                        <?php echo get_field('sort_types_desc')?>
                    </div>
                </div>
                <div class="sort-product__decor-image">
                    <img src="<?php bloginfo('template_url'); ?>/images/main/sort-decor-1.png" alt="">
                    <img src="<?php bloginfo('template_url'); ?>/images/main/sort-decor-2.png" alt="">
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
<div class="mobile-fixed-button mobile-fixed-button_active">
    <div class="mobile-fixed-button__item">
        <button data-fancybox data-src="#modal01" class="button button_mobile-fixed"> <span> Пробная сортировка </span></button>
    </div>
</div>