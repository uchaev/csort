<?php
/**
 * Template Name: Статьи
 * @package csort
 * @subpackage csort
 */


get_header(); ?>
<div class="news__list">
		<?php 
			$args = array(
			   'post_type' => 'article',
			   'numberposts' => -1,
			   'publish' => true,
			   'orderby' => 'date',
			   'order' => 'DESC',
			);
		?>
		<?php $articles = new WP_Query($args); while ($articles->have_posts()) { $articles->the_post(); ?>
			<div class="article news__list-item">
				<div class="article__info">
					<div class="article__info-top">
						<div class="article__info-top-desc">
							<div class="article__info-title">
								<h2><a href="<?php the_permalink();?>"><?php the_title();?> <i></i></a></h2>
							</div>
						</div>
					</div>
					<div class="article__info-bottom">
						<div class="article__info-desc">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php } wp_reset_postdata(); ?>
</div>
<?php get_footer(); ?>
