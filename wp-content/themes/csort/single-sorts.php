<?php
/**
 * Сортируемый продукт
 *
 * @package WordPress
 * @subpackage csort
 * @since csort 1.0
 */

get_header(); ?>
	<div class="modal" id="modal01" style="display: none;">
		<div class="modal__wrapper">
			<div class="modal__header modal__header_price">
				Запись на тестовую сортировку
			</div>
			<div js-content-main class="modal__content">
				<form js-form action="#">
					<input name="form_type" type="hidden" value="trial"/>
					<input name="form_title" type="hidden" value="Запись на тестовую сортировку"/>
					<div js-form-row class="modal__row">
						<div class="modal__grid modal__grid_label">
							<span> Ваше ФИО </span>
						</div>
						<div class="modal__grid modal__grid_input">
							<input name="product" type="text" hidden value="продукт">
							<input name="personal"  type="text">
						</div>
					</div>
					<div js-form-row class="modal__row">
						<div class="modal__grid modal__grid_label">
							<span> Телефон <sup>*</sup> </span>
						</div>
						<div class="modal__grid modal__grid_input">
							<input name="phone" js-form-required type="text">
						</div>
					</div>
					<div js-form-row class="modal__row modal__row_button">
						<div class="modal__grid modal__grid_button">
							<button type="submit" class="button button_form">Отправить</button>
						</div>
					</div>
				</form>
			</div>
			<div js-content-succes class="modal__content modal__content_hidden">
				<div js-form-row class="modal__row">
					<div class="modal__grid modal__grid_success">
						<span> Спасибо за заявку</span>
					</div>
				</div>
			</div>
			<div class="modal__footer">
				<span> Мы уверены в высокой точности работы фотосепараторов и готовы продемонстрировать её Вам. Отправьте нам Ваш неочищенный продукт, и мы проведем тестовую сортировку на выбранном Вами фотосепараторе. Таким образом, Вы получите возможность оценить эффективность фотосортировки именно Вашего продукта.</span>
			</div>
		</div>
	</div>
	<div class="sort-product">
		<div class="sort-product__wrapper sort-product__wrapper_intro">
			<div class="sort-product__main sort-product__main_intro sort-product__main_merge">
				<div class="sort-product__intro">
					<div class="sort-product__intro-title">
						<h1 class="title title_color title_large"><?php the_title();?></h1>
					</div>
					<div class="sort-product__cards">
						<a data-fancybox="sort" href="<?php echo types_render_field('sorts_img1',array('url'=>'true'))?>"
						   class="sort-product__card sort-product__card_photo">
							<div style="background-image: url('<?php echo types_render_field('sorts_img1',array('url'=>'true'))?>');"
								 class="sort-product__card-photo">

							</div>
							<div class="sort-product__card-desc">
								<span><?php echo types_render_field('sorts_img1_title',array('raw'=>'true'))?></span>
							</div>
						</a>
						<a data-fancybox="sort" href="<?php echo types_render_field('sorts_img2',array('url'=>'true'))?>"
						   class="sort-product__card sort-product__card_photo">
							<div class="sort-product__card-photo"
								 style="background-image: url('<?php echo types_render_field('sorts_img2',array('url'=>'true'))?>');">
							</div>
							<div class="sort-product__card-desc">
								<span><?php echo types_render_field('sorts_img2_title',array('raw'=>'true'))?></span>
							</div>
						</a>
						<?php if (types_render_field('sorts_vide_url',array('url'=>'true')) != NULL) { ?>
						<a data-fancybox="sort" href="<?php echo types_render_field('sorts_vide_url',array('url'=>'true'))?>" class="sort-product__card sort-product__card_video">
							<div style="background-image: url('<?php echo types_render_field('sorts_video_preview',array('url'=>'true'))?>');"
								 class="sort-product__card-video ">

							</div>
							<div class="sort-product__card-desc">
								<span><?php echo types_render_field('sorts_video_title',array('raw'=>'true'))?></span>

							</div>
						</a>
						<?php } ?>
					</div>
					<div class="sort-product__trial">
						<div class="sort-product__button">
							<button data-fancybox data-src="#modal01"
									class="button button_price button_price-mobile">
								<span class="button__icon button__icon_trial button__icon_trial-mobile"></span>
								<span>Заказать на тестовую сортировку</span>
							</button>
						</div>
						<div class="sort-product__promo sort-product__promo_desktop">
							<a data-fancybox="" href="<?php bloginfo('template_url'); ?>/images/main/promo-sort.jpg"> <img
										src="<?php bloginfo('template_url'); ?>/images/main/promo-sort.jpg" alt=""> </a>
						</div>
                        <div class="sort-product__promo sort-product__promo_mobile">
                           <img src="<?php bloginfo('template_url'); ?>/images/main/sort-step-1.png" alt="">
                           <img src="<?php bloginfo('template_url'); ?>/images/main/sort-step-2.png" alt="">
                           <img src="<?php bloginfo('template_url'); ?>/images/main/sort-step-3.png" alt="">
                        </div>

					</div>
					<div class="sort-product__text">
						<div class="text-content">
							<?php the_content();?>
						</div>
					</div>
					<?php
						$ids = get_field('equipment_relative', false, false);
						if ($ids) {
							$relatives = new WP_Query(array(
								'post_type' => 'equipment',
								'posts_per_page' => -1,
								'post__in' => $ids,
								'post_status' => 'any',
								'orderby' => 'post__in',
							));
						} else {
							$relatives = NULL;
						}
					?>
					<?php if ($relatives != NULL && $relatives->have_posts()) { ?>
					<div class="sort-product__recommended">
						<div class="recommend">
							<div class="recommend__title">
								Рекомендуем
							</div>
							<div class="recommend__wrapper">
								<?php while ($relatives->have_posts()) { $relatives->the_post(); ?>
								<a href="<?php the_permalink(); ?>" class="recommend__item">
									<div class="recommend__item-image">
										<img src="<?php echo types_render_field('equipment_img',array('url'=>'true'))?>" alt="<?php the_title();?>">
									</div>
									<div class="recommend__item-info">
										<div class="recommend__item-title"><?php the_title();?></div>
										<div class="recommend__item-link">
											<span><i></i> Подробнее</span>
										</div>
									</div>
								</a>
								<?php } wp_reset_postdata(); ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="sort-product__aside sort-product__aside_color sort-product__aside_merge">
				<?php
				$args = array(
					'post_type' => 'sort_types',
					'numberposts' => -1,
					'orderby' => 'date',
					'order' => 'ASC',
				);
				$types = new WP_Query($args);
				?>
				<?php while($types->have_posts()) { $types->the_post(); $img_url = types_render_field('sort_types_img',array('url'=>'true'));?>
					<div class="sort-product__nav ">
						<div class="sort-product__nav-title">
							<?php the_title();?>
						</div>
						<?php
						$args_sorts = array(
							'post_type' => 'sorts',
							'numberposts' => -1,
							'orderby' => 'date',
							'order' => 'ASC',
							'meta_query' => array(
								array(
									'key' => '_wpcf_belongs_sort_types_id',
									'value' => get_the_ID()
								)
							)
						);
						$type = get_post(); $sorts = new WP_Query($args_sorts);
						$title = get_the_title();
						?>
						<?php if ($types) { ?>
							<div class="sort-product__nav-list">
								<div class="sort-product__nav-item">
									<?php while ($sorts->have_posts()) { $sorts->the_post(); ?>
										<div class="sort-product__nav-link">
											<a href="<?php the_permalink();?>"><?php echo types_render_field('sorts_name',array('raw'=>'true'))?></a>
										</div>
									<?php } $sorts->reset_postdata(); setup_postdata($type); ?>


									<div class="sort-product-mobile sort-product__nav-mobile">
										<div class="sort-product-mobile__title">
											<div class="sort-product-mobile__title-left">
												<?php echo $title; ?>
											</div>
										</div>
										<div class="sort-product-mobile__list">
											<?php while ($sorts->have_posts()) { $sorts->the_post(); ?>
												<a href="<?php the_permalink()?>"  class="sort-product-mobile__item">
													<div class="sort-product-mobile__item-image"
														 style="background-image: url('<?php echo types_render_field('sorts_img1',array('url'=>'true'))?>')">
														<div class="sort-product-mobile__item-label"><?php echo types_render_field('sorts_name',array('raw'=>'true'))?></div>
													</div>
												</a>
											<?php } $sorts->reset_postdata(); setup_postdata($type); ?>
										</div>
									</div>

								</div>
							</div>
						<?php } ?>
					</div>
				<?php } wp_reset_postdata();?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
<div class="mobile-fixed-button mobile-fixed-button_active ">
    <div class="mobile-fixed-button__item">
        <button data-fancybox data-src="#modal01" class="button button_mobile-fixed"> <span> Пробная сортировка </span></button>
    </div>
</div>

