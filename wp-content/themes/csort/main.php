<?php
/**
 * Template Name: Главная
 * @package csort
 * @subpackage csort
 */
  get_header() ?>

    <div class="slider slider_mobile">
        <?php
        $args = array(
            'post_type' => 'mobile-slider',
            'publish' => true,
            'orderby' => 'meta_value',
            'order' => 'ASC',
        );
        ?>
        <div js-slider-mobile class="owl-carousel owl-theme">
            <?php $index = 0; $banners = new WP_Query($args); if ($banners->have_posts()): while ($banners->have_posts()): $banners->the_post(); ?>
                <div class="item">
                    <div class="slider__item">
                        <a class="slider__item-link" href="<?php echo types_render_field('mobile-slider_url',array('row'=>'true'))?>" title="<?php the_title();?>">
                            <div style="background-image: url('<?php echo types_render_field('mobile-slide_img',array('url'=>'true'))?>')" class="slider__item-image slider__item-image_mobile"> </div>
                        </a>
                    </div>
                </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
    </div>


	<div class="slider slider_desktop">
		<?php 
			$args = array(
			   'post_type' => 'slider',
			   'publish' => true,
			   'orderby' => 'meta_value',
			   'order' => 'ASC',
			);
		?>
		<div js-slider-main class="owl-carousel owl-theme">
			<?php $index = 0; $banners = new WP_Query($args); if ($banners->have_posts()): while ($banners->have_posts()): $banners->the_post(); ?>
				<div class="item">
				<div class="slider__item">
					<a class="slider__item-link" href="<?php echo types_render_field('slider_url',array('row'=>'true'))?>" title="<?php the_title();?>">
						<img src="<?php echo types_render_field('slide_img',array('url'=>'true'))?>" alt="<?php the_title();?>"
							 class="slider__item-image slider__item-image_desktop">
					</a>
				</div>
			</div>
			<?php endwhile; endif; wp_reset_postdata(); ?>
		</div>
	</div>
	<div class="content__main">
		<div class="content__section content__section_intro">
			<div class="category category_intro">
				<div class="category__title">
					<h2 class="title title_color title_regular">Фотосепараторы</h2>
					<span js-category-card-video-title>Видео Сортировки</span>
				</div>

				<div class="category__wrapper category__wrapper_intro">
					<?php 
						$args = array(
						   'post_type' => 'catalog',
						   'publish' => true,
						   'orderby' => 'date',
						   'order' => 'ASC',
						   'posts_per_page' => '3',
						   'meta_query' => array(
								array(
									'key' => 'wpcf-catalog_index',
									'value' => '1'
								)
							)
						);
					?>
					<?php $index = 0; $catalog = new WP_Query($args); if ($catalog->have_posts()): while ($catalog->have_posts()): $catalog->the_post(); ?>
						<div class="category-card category-card_intro category__item">
						<div class="category-card__wrapper">
							<?php if (types_render_field('equipment_go_to_url_check', array('raw' => 'true'))) { ?>
							<a href="<?php echo types_render_field('equipment_go_to_url', array('raw' => 'true'))?>" title="<?php the_title();?>">
							<?php } else { ?>
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
							<?php } ?>
								<div class="category-card__image">
									<img src="<?php echo types_render_field('catalog_img',array('url'=>'true'))?>" alt="<?php the_title();?>" title="<?php the_title();?>">
								</div>
								<div class="category-card__desc">
									<span><?php the_title();?><i> </i></span>
								</div>
							</a>
						</div>
					</div>
					<?php endwhile; endif; wp_reset_postdata(); ?>
					<div js-category-card-video
						 class="category-card category-card_intro category-card_video category__item">
						<div class="category-card__wrapper">
							<a href="#" target="_blank" title="">
								<div class="category-card__image category-card__image-video">
									<img src="<?php bloginfo('template_url'); ?>/images/temp/video-preview.jpg" alt="">
									<div class="category-card__play">

									</div>
								</div>
								<div class="category-card__title">
									Видео сортировки
								</div>
								<div class="category-card__desc">
									<span> Видео ролики сортировки продуктов на фотосепараторах</span>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content__section content__section_main">
			<div class="aside  aside_news aside_left">
				<div class="news">
					<div class="news__wrapper">
						<div class="news__title">
							<span class="title title_regular title_color">Новости</span>
						</div>
						<div class="news__list">
							<?php 
								$limit = 4;
								$args = array(
								   'post_type' => 'news',
								   'publish' => true,
								   'orderby' => 'date',
								   'order' => 'date',
								   'posts_per_page' => $limit,
								);
							?>
							<?php $news = new WP_Query($args); while ($news->have_posts()) { $news->the_post(); ?>
								<div class="news-item news__list-item">
									<div class="news-item__title news-item__title_date"><?php the_date();?></div>
									<div class="news-item__desc">
										<a href="<?php the_permalink();?>" alt="<?php the_title();?>"><?php the_title();?><i></i></a>
									</div>
								</div>
							<?php } wp_reset_postdata(); ?>
						</div>
					</div>
					<div class="news__link">
						<a href="/new">Все новости</a>
					</div>
				</div>
			</div>
			<div class="main">
				<div class="intro">
					<div class="intro__title">
						<h2 class="title title_regular title_color">
							О Компании
						</h2>
					</div>
					<div class="intro__content">
						<?php echo get_the_content(); ?>
					</div>
				</div>
			</div>
			<div class="aside aside_label aside_right">
				<div class="label">
					<div class="label__wrapper">
						<div class="label__title">
							<span class="title title_color title_regular">Технология</span>
						</div>
						<div class="label__desc">
						<span>Фотосепарация (с англ. colour sorting – цветосортировка, сортировка по цвету) – 
							технология сортировки любых сыпучих материалов, основанная на анализе продукта по цвету.
							Последние годы эта технология завоевывает все большее признание российских производителей.</span>
							<?php 
								$limit = 5;
								$args = array(
								   'post_type' => 'technology',
								   'publish' => true,
								   'orderby' => 'date',
								   'order' => 'ASC',
								   'posts_per_page' => $limit,
								);
							?>
							<ul type="none">
								<?php $tech = new WP_Query($args); while ($tech->have_posts()) { $tech->the_post(); ?>
									<li><a href="<?php the_permalink();?>"><?php the_title(); ?></a></li>
								<?php } wp_reset_postdata(); ?>
							</ul>
						</div>
					</div>
					<div class="label__link">
						<a href="/technology/photoseparator/" alt="Тенологии">Подробнее</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer() ?>