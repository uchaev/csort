$(function () {
    $('[js-slider-main]').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive:{
            0:{
                items:1
            }
        }
    });

    $('[js-slider-mobile]').owlCarousel({
        loop: true,
        margin: 0,
        dots:false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive:{
            0:{
                items:1
            }
        }
    });
});