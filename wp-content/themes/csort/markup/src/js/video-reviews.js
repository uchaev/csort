$(function () {
    $(function () {
        $('[js-video-reviews]').owlCarousel({
            loop: true,
            margin: 0,
            autoplay: true,
            autoplayTimeout: 2500,
            autoplayHoverPause: true,
            dots: true,
            responsive: {
                0: {
                    items: 1
                }
            }
        });
    })
})