$(function () {
    $(window).scroll(function () {
        review()
    });

    function review() {
        $('[js-mobile-review]').each(function () {
            var srcGif = $(this).data('src-gif');
            var srcImage = $(this).data('src-image');
            if ($(this).isInView()) {
                if ($(this).attr('src') != srcGif) {
                    $(this).attr('src', srcGif);
                }
            }
            else {
                $(this).attr('src', srcImage)
            }
        })
    }
});

/*
 */
