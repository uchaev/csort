$(function () {

    let $window = $(window),
        lastScrollTop = 0;


    let onScroll = (e) => {
        let top = $window.scrollTop();
        if (lastScrollTop > top) {
            $('.header-mobile__top').show();

        } else if (lastScrollTop < top) {
            $('.header-mobile__top').hide();
        }
        lastScrollTop = top;
    };

    $window.scroll(function () {
       if($window.scrollTop() < 100) {
           $('.header-mobile__top').show();
           return false
       }
       onScroll()
    });

    // $window.on('scroll', onScroll);

});