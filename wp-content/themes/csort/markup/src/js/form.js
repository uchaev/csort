$(function () {
    $('[js-form]').submit(function (e) {
        var $form = $(this);
        $form.find('[js-form-required]').each(function (e) {
            $(this).closest('[js-form-row]').removeClass('is-error');
            var valInput = $(this).val();
            if (!valInput) {
                $(this).closest('[js-form-row]').addClass('is-error');
            }
        });
        if ($form.find('.is-error').length !== 0) {
            e.preventDefault();
            return false;
        }
        else {
            $.ajax({
                type: "POST",
                url: " /wp-admin/admin-ajax.php",
                data: $form.serialize() + "&pageLink=" + window.location + "&action=send_feedback"
            });

            $form.closest('.modal').find('[js-content-main]').addClass('modal__content_hidden');
            $form.closest('.modal').find('[js-content-succes]').removeClass('modal__content_hidden');


            setTimeout(function () {
                $.fancybox.close();
                setTimeout(function () {
                    $form.trigger('reset');
                    $form.closest('.modal').find('[js-content-main]').removeClass('modal__content_hidden');
                    $form.closest('.modal').find('[js-content-succes]').addClass('modal__content_hidden');
                }, 1500)
            }, 3000);
            return false
        }
    });
});