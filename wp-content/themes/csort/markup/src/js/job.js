$(function () {
    var fixed = function () {
        setTimeout(function () {
            if ($(window).width() <= 650) {
                $('.zarplataWidget-card__column').attr('style', 'float:none!important;width:100%!important');
                $('.zarplataWidget-card__text-right').attr('style', 'margin-top:20px!important');
            }
            else {
                $('.zarplataWidget-card__column').attr('style', '');
                $('.zarplataWidget-card__text-right').attr('style', '');
            }
        }, 1000)
    };
    fixed();
    $(window).resize(function () {
        fixed()
    })
});