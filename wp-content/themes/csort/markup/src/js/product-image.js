$(function () {

    var $owl = $('[js-product-gallery-list]');

    $owl.owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        dots:false,
        responsive: {
            0: {
                items: 1
            }
        }
    });


    $('[js-product-gallery-prev]').on('click',function () {
        $owl.trigger('prev.owl.carousel')
    })

    $('[js-product-gallery-next]').on('click',function () {
        $owl.trigger('next.owl.carousel')
    })
});