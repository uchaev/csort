$(function () {
    var $button = $('[js-mobile-button]');
    $(window).scroll(function () {
        if ($button.isInView() == false) {
            $('[js-mobile-fixed-button]').addClass('mobile-fixed-button_active')
        }
        else {
            $('[js-mobile-fixed-button]').removeClass('mobile-fixed-button_active')
        }
    })
});