$(function () {
    $('[js-tab]').on('click', function (e) {
        e.preventDefault();
        $('[js-tab]').removeClass('product-tabs__header-item_active');

        $(this).addClass('product-tabs__header-item_active');
        var number = $(this).attr('js-tab');

        $('[js-tab-content]').removeClass('product-tabs__content_show');
        $('[js-tab-content]').each(function () {
            if ($(this).attr('js-tab-content') == number) {
                $(this).addClass('product-tabs__content_show')
            }
        });

        $('[js-product-tabs-mobile]').removeClass('product-tabs__mobile-header_show');
        $('[js-product-tabs-mobile]').each(function () {
            if ($(this).attr('js-product-tabs-mobile') == number) {
                $(this).addClass('product-tabs__mobile-header_show')
            }
        });

    });

    $('[js-product-tabs-mobile]').on('click', function (e) {


        let number = $(this).attr('js-product-tabs-mobile');


        e.preventDefault();

        $(this).toggleClass('product-tabs__mobile-header_show');
        $(this).next().toggleClass('product-tabs__content_show');
        $('[js-product-tabs-mobile]').toggleClass('product-tabs__mobile-header_show');


      /*  $('[js-tab-content]').each(function () {
            if ($(this).attr('js-tab-content') == number) {
                $(this).addClass('product-tabs__content_show')
            }
        });*/



        /*$('[js-product-tabs-mobile]').each(function () {
            if ($(this).attr('js-product-tabs-mobile') == number) {
                $(this).addClass('product-tabs__mobile-header_show')
            }
        });
        */
/*
        $('[js-tab]').removeClass('product-tabs__header-item_active');

        $('[js-tab]').each(function () {
            if ($(this).attr('js-tab') == number) {
                $(this).addClass('product-tabs__header-item_active')
            }
        })*/
    })

    function closeTabs() {
        $('[js-product-tabs-mobile]').removeClass('product-tabs__mobile-header_show');
        $('[js-tab-content]').removeClass('product-tabs__content_show')

    }

    function openTabs() {
        $('[js-product-tabs-mobile="1"]').addClass('product-tabs__mobile-header_show');
        $('[js-tab-content="1"]').addClass('product-tabs__content_show')
        $('[js-tab]').removeClass('product-tabs__header-item_active"')
        $('[js-tab="1"]').removeClass('product-tabs__header-item_active"')
    }


    if ($(window).width() <= 650) {

        closeTabs();
    }
    else {
        openTabs()
    }

    $(window).resize(function () {
        if ($(window).width() <= 650) {
            closeTabs();
        }
        else {
            openTabs()
        }
    })
});