$(function () {

    // custom scrollbar
    $('[js-burger-siderbar]').mCustomScrollbar({
        axis: "y", // horizontal scrollbar
        theme: "dark"
    });

    // open sidebar
    $('[js-burger]').on('click', function () {
            $('[js-burger-siderbar]').addClass('header-mobile__sidebar_open');
            $('[js-header-mobile-middle]').addClass('header-mobile__middle_hide')
        }
    )

    $('[js-close-mobile-sidebar]').on('click', function () {
        $('[js-burger-siderbar]').removeClass('header-mobile__sidebar_open');
        $('[js-header-mobile-middle]').removeClass('header-mobile__middle_hide')
    })

})