$(function () {
    var $video = $('[js-category-card-video]');
    var $videoTitle = $('[js-category-card-video-title]');
    calculate();

    $(window).resize(function () {
        calculate();
    });

    function calculate() {
        var videoWidth = $video.width();
        $videoTitle.width(videoWidth)
    }
})