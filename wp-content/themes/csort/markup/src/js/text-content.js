$(function () {

    var maxWidth = $('.content__wrapper').width();
    var asideWidth = $('.product__aside').outerWidth();
    var contentWidth = maxWidth - asideWidth - 50;

    $('.text-content_product table').each(function () {

        var $table = $(this);
        $table.wrap('<div class="table table_product" style="max-width:' + contentWidth + 'px"> </div>')

    });

    $('.text-content_product iframe').each(function () {
        var $iframe = $(this);
        $iframe.wrap('<div class="youtube youtube_product" style="max-width:' + contentWidth + 'px"> </div>')
    });

    $(window).resize(function () {
        if ($(window) <= 650) {
            return false
        }
        var maxWidth = $('.content__wrapper').outerWidth();
        var asideWidth = $('.product__aside').outerWidth();
        var contentWidth = maxWidth - asideWidth - 50;

        $('.text-content_product .table_product').each(function () {
            var $table = $(this);
            $table.css('max-width', contentWidth)
        });

        $('.text-content_product .youtube_product').each(function () {
            var $iframe = $(this);
            $iframe.css('max-width', contentWidth)
        });
    });


});