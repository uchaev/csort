<?php
/**
 * Template Name: Фотогалерея
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
	<div class="photo">
		<div class="photo__title">
			<h1 class="title title_color title_large"><?php the_title(); ?></h1>
		</div>
		<div class="photo__list">
			<?php the_content();?>
		</div>
	</div>                
<?php get_footer(); ?>
