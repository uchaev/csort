<?php
/**
 * Template Name: Контакты
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
<div class="modal" id="trial" style="display: none;">
    <div class="modal__wrapper">
        <div class="modal__header modal__header_price">
            Запись на тестовую сортировку
        </div>
        <div js-content-main class="modal__content">
            <form js-form js-form-contact action="#">
                <input name="form_type" type="hidden" value="trial"/>
                <input name="form_title" type="hidden" value="Запись на тестовую сортировку"/>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Ваше ФИО</span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="product" type="text" hidden value="продукт">
                        <input name="personal" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> E-mail</span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="email" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Телефон <sup>*</sup> </span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="phone" js-form-required type="text">
                    </div>
                </div>

                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Опишите,  в чем состоитзадача сортировки(продукт, загрязнитель <sup>*</sup> </span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <textarea name="comment" js-form-required type="text"></textarea>
                    </div>
                </div>


                <div js-form-row class="modal__row modal__row_button">
                    <div class="modal__grid modal__grid_button">
                        <button type="submit" class="button button_form">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
        <div js-content-succes class="modal__content modal__content_hidden">
            <div js-form-row class="modal__row">
                <div class="modal__grid modal__grid_success">
                    <span> Спасибо за заявку</span>
                </div>
            </div>
        </div>
        <div class="modal__footer">
            <span>
                Мы уверены в высокой точности работы фотосепараторов и
                готовы продемонстрировать её Вам.
                Отправьте нам Ваш неочищенный продукт, и мы проведем тестовую
                сортировку на выбранном Вами фотосепараторе.
                Таким образом, Вы получите возможность оценить эффективность
                фотосортировки именно Вашего продукта. </span>
        </div>
    </div>
</div>

<div class="modal" id="callback" style="display: none;">
    <div class="modal__wrapper">
        <div class="modal__header modal__header_price">
            Для заказа консультации, пожалуйста, заполните поля формы
        </div>
        <div js-content-main class="modal__content">
            <form js-form js-form-contact action="#">
                <input name="form_type" type="hidden" value="callback"/>
                <input name="form_title" type="hidden" value="Заказ обратного звонка"/>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Ваше ФИО</span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="personal" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span><sup>*</sup>  Удобные для Вас дата и время (по Москве) звонка:</span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="time" js-form-required type="text">
                    </div>
                </div>

                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Телефон <sup>*</sup> </span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="phone" js-form-required type="text">
                    </div>
                </div>

                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Какой вопрос Вас интересует <sup>*</sup> </span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <textarea name="comment" js-form-required type="text"></textarea>
                    </div>
                </div>

                <div js-form-row class="modal__row modal__row_button">
                    <div class="modal__grid modal__grid_button">
                        <button type="submit" class="button button_form">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
        <div js-content-succes class="modal__content modal__content_hidden">
            <div js-form-row class="modal__row">
                <div class="modal__grid modal__grid_success">
                    <span> Спасибо за заявку</span>
                </div>
            </div>
        </div>
        <div class="modal__footer">
            <span> Оставьте Ваши координаты и мы свяжемся с Вами в ближайшее время </span>
        </div>
    </div>
</div>

<div class="modal" id="consultation" style="display: none;">
    <div class="modal__wrapper">
        <div class="modal__header modal__header_price">
            Заявка на консультацию
        </div>
        <div js-content-main class="modal__content">
            <form js-form-contact js-form action="#">
                <input name="form_type" type="hidden" value="consultation"/>
                <input name="form_title" type="hidden" value="Заявка на консультацию"/>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Тема консультации</span>
                    </div>
                    <?php
                    $args = array(
                        'post_type' => 'consultation_theme',
                        'numberposts' => -1,
                        'orderby' => 'date',
                        'order' => 'desc',
                    );
                    $themes = new WP_Query($args);
                    $first = true;
                    ?>
                    <div class="modal__grid modal__grid_input modal__grid_select">
                        <select name="consultation" nice-select js-form-required name="" id="">
                            <?php while ($themes->have_posts()) {
                                $themes->the_post(); ?>
                                <option value="<?php the_title(); ?>" <?php if ($first) {
                                    echo 'selected';
                                    $first = false;
                                } ?>>
                                    <?php the_title(); ?>
                                </option>
                            <?php }
                            wp_reset_postdata(); ?>
                        </select>
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Ваше ФИО</span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="personal" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> E-mail</span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="email" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Телефон <sup>*</sup> </span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <input name="phone" js-form-required type="text">
                    </div>
                </div>
                <div js-form-row class="modal__row">
                    <div class="modal__grid modal__grid_label">
                        <span> Примечание или ваш вопрос <sup>*</sup> </span>
                    </div>
                    <div class="modal__grid modal__grid_input">
                        <textarea name="comment" js-form-required type="text"></textarea>
                    </div>
                </div>
                <div js-form-row class="modal__row modal__row_button">
                    <div class="modal__grid modal__grid_button">
                        <button type="submit" class="button button_form">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
        <div js-content-succes class="modal__content modal__content_hidden">
            <div js-form-row class="modal__row">
                <div class="modal__grid modal__grid_success">
                    <span> Спасибо за заявку</span>
                </div>
            </div>
        </div>
        <div class="modal__footer">
            <span> Оставьте Ваши координаты и мы свяжемся с Вами в ближайшее время </span>
        </div>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li><a href="<?php the_permalink(); ?>" class="is-active"> <i></i> Контакты</a></li>
    </ul>
</div>
<div class="contact">
     <div class="contact__buttons">
        <button data-fancybox data-src="#trial" class="button button_contact button_contact-first"><span>Запись на пробную сортировку</span></button>
        <button data-fancybox data-src="#callback" class="button button_contact"><span>Заказ обратного звонка</span></button>
        <button data-fancybox data-src="#consultation" class="button button_contact"><span>Заявка на консультацию</span></button>
    </div>
    <div class="contact__title">
        <h1 class="title title_regular title_color">Единый номер сервисной службы </h1>
        <span>
			   <a href="tel:<?php echo get_option('phone_input_setting'); ?> "> <?php echo get_option('phone_input_setting'); ?> </a> (звонки только для абонентов из РФ, бесплатно)
			</span>
    </div>
    <div class="contact__list">
        <?php
        $args = array(
            'post_type' => 'contacts',
            'publish' => true,
            'numberposts' => -1,
            'orderby' => 'date',
            'order' => 'DESC',
        );
        ?>
        <?php $contacts = new WP_Query($args);
        while ($contacts->have_posts()) {
            $contacts->the_post(); ?>
            <div class="contact__list-item contact-item">
                <div class="contact-item__image">
                    <img src="<?php echo types_render_field('contacts_img', array('url' => 'true')) ?>"
                         alt="<?php the_title(); ?>">
                </div>
                <div class="contact-item__desc">
                    <div class="contact-item__desc-wrapper">
                        <div class="contact-item__info">
                            <div class="contact-item__info-name">
                                <p><?php echo types_render_field('contacts_lastname', array('raw' => 'true')) ?></p>
                                <p><?php echo types_render_field('contacts_firstname', array('raw' => 'true')) ?> <?php echo types_render_field('contacts_middlename', array('raw' => 'true')) ?></p>
                            </div>
                            <div class="contact-item__info-work">
                                <?php echo types_render_field('contacts_position', array('raw' => 'true')) ?>
                            </div>
                        </div>
                        <div class="contact-item__additionally">
                            <div class="contact-item__additionally-wrapper">
                                <div class="contact-item__additionally-address">
                                    <p class="is-city"><?php echo types_render_field('contacts_city', array('raw' => 'true')) ?></p>
                                    <p class="is-street"><?php echo types_render_field('contacts_address', array('raw' => 'true')) ?></p>
                                </div>
                                <div class="contact-item__additionally-list">
                                    <div class="contact-item__additionally-item">
                                        <span><a href="tel:+7 (3852) 29-03-10">Тел./факс <?php echo types_render_field('contacts_fax', array('raw' => 'true')) ?></a></span>
                                    </div>
                                    <div class="contact-item__additionally-item contact-item__additionally-item_border">
                                        <p>
                                            <span><a href="tel:<?php echo types_render_field('contacts_phone1', array('raw' => 'true')) ?>"> Тел.: <?php echo types_render_field('contacts_phone1', array('raw' => 'true')) ?></a></span>
                                        </p>
                                        <p>
                                            <span><a href="tel:<?php echo types_render_field('contacts_phone2', array('raw' => 'true')) ?>"><?php echo types_render_field('contacts_phone2', array('raw' => 'true')) ?></a></span>
                                        </p>
                                    </div>
                                    <div class="contact-item__additionally-item contact-item__additionally-item_border">
                                        <span><a href="mailto:<?php echo types_render_field('contacts_email', array('raw' => 'true')) ?>"
                                                 class="is-email">E-mail: <?php echo types_render_field('contacts_email', array('raw' => 'true')) ?></a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
        wp_reset_postdata(); ?>
    </div>
</div>
<?php get_footer(); ?>
