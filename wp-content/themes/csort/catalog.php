<?php
/**
 * Template Name: Каталог оборудования
 * @package csort
 * @subpackage csort
 */

get_header(); ?>
	<div class="catalog">
		<div class="catalog__category">
			<div class="category category_intro">
				<div class="category__title">
					<div class="title title_regular title_color">Выберите интересующую Вас линию
						оборудования:
					</div>
				</div>

				<div class="category__wrapper category__wrapper_catalog category__wrapper_intro">
					<?php 
						$args = array(
						   'post_type' => 'catalog',
						   'publish' => true,
						   'numberposts' => -1,
						   'orderby' => 'date',
						   'order' => 'ASC',
						   'limit' => '4',
						);
					?>
					<?php $index = 0; $catalog = new WP_Query($args); if ($catalog->have_posts()): while ($catalog->have_posts()): $catalog->the_post(); ?>
						<div class="category-card category-card_intro category__item">
							<div class="category-card__wrapper">
								<?php if (types_render_field('equipment_go_to_url_check', array('raw' => 'true'))) { ?>
								<a href="<?php echo types_render_field('equipment_go_to_url', array('raw' => 'true'))?>" title="<?php the_title();?>">
								<?php } else { ?>
								<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<?php } ?>
									<div class="category-card__image">
										<img src="<?php echo types_render_field('catalog_img',array('url'=>'true'))?>" alt="<?php the_title();?>" title="<?php the_title();?>">
									</div>
									<div class="category-card__desc">
										<span><?php the_title();?><i> </i></span>
									</div>
								</a>
							</div>
						</div>
					<?php endwhile; endif; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<div class="catalog__desc">
			<div class="text-content">
				<?php the_content();?>
			</div>
		</div>
		<div class="bonus catalog__bonus">
			<div class="bonus__item">
				<div class="bonus__item-img">
					<img src="<?php bloginfo('template_url'); ?>/images/main/dealer-icon-grey.png" alt="  ">
				</div>
				<div class="bonus__item-info">
					<div class="bonus__item-title">
						ДИЛЕРЫ
					</div>
					<div class="bonus__item-desc">
						Продажа фотосепараторов "СиСорт" в различных странах мира возможна с
						участием дилеров компании. Обратитесь к нам, чтобы узнать, как связаться с
						дилером в Вашей стране.
					</div>
				</div>
			</div>
			<div class="bonus__item">
				<div class="bonus__item-img">
					<img src="<?php bloginfo('template_url'); ?>/images/main/leasing-icon-grey.png" alt="  ">
				</div>
				<div class="bonus__item-info">
					<div class="bonus__item-title">
						ЛИЗИНГ
					</div>
					<div class="bonus__item-desc">
						Для Вашего удобства мы сотрудничаем с финансовыми организациями,
						предоставляющими лизинг для наших клиентов на выгодных условиях.
					</div>
				</div>
			</div>
		</div>
	</div>                  
<?php get_footer(); ?>
