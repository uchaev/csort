<?php
/**
 * Template Name: Наши награды
 * @package csort
 * @subpackage csort
 */

get_header(); ?>  
  <div class="award">
	<div class="award__title"><h1 class="title title_color title_large">Награды и достижения</h1></div>
	<div class="award__list">
		<?php 
			$args = array(
			   'post_type' => 'reward',
			   'publish' => true,
			   'numberposts' => -1,
			   'orderby' => 'date',
			   'order' => 'DESC',
			   'meta_query' => array(
								array(
									'key' => 'wpcf-reward_horizont',
									'value' => '1',
								)
							)
			);
		?>
		<div class="award__list-top">
			<?php $award = new WP_Query($args); while ($award->have_posts()) { $award->the_post(); ?>
				<a data-fancybox="award" href="<?php echo types_render_field('reward_img',array('url'=>'true'))?>" class="award__list-item award__list-item_main">
					<img src="<?php echo types_render_field('reward_img',array('url'=>'true'))?>" alt="<?php the_title();?>">
				</a>
			<?php } wp_reset_postdata(); ?>
		</div>
		<?php 
			$args = array(
			   'post_type' => 'reward',
			   'publish' => true,
			   'numberposts' => -1,
			   'orderby' => 'date',
			   'order' => 'DESC',
			   'meta_query' => array(
								array(
									'key' => 'wpcf-reward_horizont',
									'value' => '1',
									'compare' => '!=',
								)
							)
			);
		?>
		<div class="award__list-main">
			<?php $award = new WP_Query($args); while ($award->have_posts()) { $award->the_post(); ?>
				<a data-fancybox="award" href="<?php echo types_render_field('reward_img',array('url'=>'true'))?>" class="award__list-item">
					<img src="<?php echo types_render_field('reward_img',array('url'=>'true'))?>" alt="<?php the_title();?>">
				</a>
			<?php } wp_reset_postdata(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
