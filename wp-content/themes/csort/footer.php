<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage csort
 * @since csort 1.0
 */
?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer block">
				<div class="footer__wrapper block__wrapper">
					<div class="footer__info">
						<div class="footer__col footer__col_sitemap">

						</div>
						<div class="footer__col footer__col_link">
							<div class="footer__title">КАТАЛОГ ОБОРУДОВАНИЯ</div>
							<div class="footer__links">
								<ul>
									<?php 
										$args = array(
										   'post_type' => 'catalog',
										   'publish' => true,
										   'orderby' => 'date',
										   'order' => 'ASC',
										);
									?>
									<?php $catalog = new WP_Query($args); while ($catalog->have_posts()) { $catalog->the_post(); ?>
										<li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
									<?php } ?>
								</ul>
							</div>
						</div>
						<div class="footer__col footer__col_share">
							<div class="footer__title">
								ПРИСОЕДИНЯЙТЕСЬ К НАМ!
							</div>
							<div class="footer__social">
								<div class="footer__social-item">
									<a href="<?php echo get_option('facebook_input_setting'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/social/fb.png" alt="vk"></a>
								</div>
								<div class="footer__social-item">
									<a href="<?php echo get_option('vk_input_setting'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/social/vk.png" alt="vk"></a>
								</div>
								<div class="footer__social-item">
									<a href="<?php echo get_option('twitter_input_setting'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/social/tw.png" alt="vk"></a>
								</div>
								<div class="footer__social-item">
									<a href="<?php echo get_option('youtube_input_setting'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/social/youtube.png" alt="vk"></a>
								</div>
								<div class="footer__social-item">
									<a href="<?php echo get_option('linkedin_input_setting'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/social/in.png" alt="vk"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="footer__bottom">
						<span>© 2010 – 2017, Все права защищены </span>
					</div>
				</div>
        </div>
    </body>
</html>
