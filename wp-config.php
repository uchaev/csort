<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'csort-dev');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3)~TwD<BA`RG()x#LF+I`a0_t|z}|o`$*Ng&T2!4]4e{t%3?0643*+FVsZpG3uvi');
define('SECURE_AUTH_KEY',  'KBFsY:b_YGcO.Cx iJK9sv$Iq609F-g`_iHpseyB;2Q+V/yA^y~Lkq~^/*Ka@uKT');
define('LOGGED_IN_KEY',    'px>nf49^a&rw5y>Sr .uMt*Bs30#H<AoEs%Rec?uzyh0Vq&n{BrH*)v:NW;hLqbO');
define('NONCE_KEY',        '$m06wmI|Ns&+C<]T$`VNjNu[1[TU{h;?5m&sv-uC[#34^&4DT|x,G,!K4z2/PJ]g');
define('AUTH_SALT',        '_m?cM{Y3a#@_5mD3`DY]ntBSO@BL,=2&t;d?iH0=diAa{yt8}REB:+x,Tf_:RGyC');
define('SECURE_AUTH_SALT', 'jDj_/@K=hWD_rP`U$uj}|T%}?JC%*Oa(mZ`<n-JF~xU_HYe>5MG{`5eGB7]6S=W`');
define('LOGGED_IN_SALT',   '2`nwLpu+$BnCs(^o`]VW31,p%A1.X5R,TrPPU:40d9N=k]3:[$1z=^CyD(&a%5^$');
define('NONCE_SALT',       ':U4@-sw$]|= ;A:t()BSqt_}QO,;bFae;~PCu/TH|uI}N7d`]JtN>mWIq[%.AsoF');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
